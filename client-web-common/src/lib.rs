use common::MessageFromTheBoardValue;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct GenRandomAnswer {
    pub value: u32,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct PingAnswer {
    pub pong_value: u32,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ToggleLedAnswer {}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ReadAdcAnswer {
    pub adc_value: u16,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct SetRgbLedAnswer {}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct MessageFromJsonAnswer {
    pub answer_value: MessageFromTheBoardValue,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ReadTemepratureAndHumidityAnswer {
    pub temperature: f32,
    pub humidity: f32,
}
