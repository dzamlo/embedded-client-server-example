use gloo_net::http::Request;
use leptos::error::Result;
use leptos::*;
use serde::de::DeserializeOwned;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("server error ({status}): {text}")]
    ServerError { status: u16, text: String },
    #[error("unknow error")]
    Unknown,
}

pub fn errors_fallback(errors: RwSignal<Errors>) -> impl IntoView {
    let error_list = move || {
        errors.with(|errors| {
            errors
                .iter()
                .map(|(_, e)| view! { <li>{e.to_string()}</li> })
                .collect_view()
        })
    };

    view! {
        <div class="error">
            <h3>"Error"</h3>
            <ul>{error_list}</ul>
        </div>
    }
}

pub async fn post_json<T: DeserializeOwned>(url: &str) -> Result<T> {
    let response = Request::post(url).send().await?;
    if response.ok() {
        let answer = response.json::<T>().await?;
        Ok(answer)
    } else {
        Err(Error::ServerError {
            status: response.status(),
            text: response.text().await?,
        }
        .into())
    }
}

#[component]
pub fn Loading() -> impl IntoView {
    view! { <p>"Loading" <em class="loading-animation">"..."</em></p> }
}
