use futures::StreamExt;
use gloo_net::eventsource::futures::EventSource;
use gloo_timers::future::TimeoutFuture;
use leptos::html::Div;
use leptos::*;
use leptos_use::{use_mutation_observer_with_options, UseMutationObserverOptions};
use log::*;

use crate::gen_random::GenRandom;
use crate::ping::Ping;
use crate::read_adc::ReadAdc;
use crate::read_temperature_and_humidity::TemperatureAndHumidity;
use crate::set_rgb_led::SetRgbLed;
use crate::toggle_led::ToggleLed;

mod gen_random;
mod ping;
mod read_adc;
mod read_temperature_and_humidity;
mod set_rgb_led;
mod toggle_led;
mod utils;

#[component]
fn Logs() -> impl IntoView {
    let div_element = create_node_ref::<Div>();

    spawn_local(async move {
        loop {
            if let Ok(mut es) = EventSource::new("/api/logs") {
                if let Ok(mut logs_stream) = es.subscribe("message") {
                    while let Some(Ok((_event_type, msg))) = logs_stream.next().await {
                        let log = msg.data().as_string().unwrap();
                        if let Some(div_element) = div_element() {
                            let _ = div_element.child(view! { <pre inner_html=log></pre> });
                        }
                    }
                }
            }
            // TODO: better rety timing stategy
            // Wait before retrying
            TimeoutFuture::new(1_000).await;
        }
    });

    view! { <div node_ref=div_element></div> }
}

/// main component
#[component]
fn App() -> impl IntoView {
    info!("App starting");

    let div_bottom_element = create_node_ref::<Div>();

    use_mutation_observer_with_options(
        div_bottom_element,
        move |_, _| {
            if let Some(element) = div_bottom_element() {
                element.set_scroll_top(i32::MAX);
            }
        },
        UseMutationObserverOptions::default()
            .child_list(true)
            .subtree(true),
    );

    view! {
        <div id="div-global">
            <div id="div-top">
                <h1>Embedded board control interface</h1>
                <div id="tool-container">
                    <ToggleLed/>
                    <Ping/>
                    <ReadAdc/>
                    <SetRgbLed/>
                    <GenRandom/>
                    <TemperatureAndHumidity/>
                </div>
            </div>
            <div id="div-bottom" node_ref=div_bottom_element>
                <Logs/>
            </div>
        </div>
    }
}

fn main() {
    _ = console_log::init_with_level(log::Level::Debug);
    console_error_panic_hook::set_once();

    leptos::mount_to_body(|| view! { <App/> })
}
