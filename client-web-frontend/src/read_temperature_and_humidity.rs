use client_web_common::ReadTemepratureAndHumidityAnswer;
use leptos::error::Result;
use leptos::*;

use crate::utils::*;

async fn read_temp_and_humidity() -> Result<ReadTemepratureAndHumidityAnswer> {
    post_json("/api/read_temperature_and_humidity").await
}

#[component]
pub fn TemperatureAndHumidity() -> impl IntoView {
    let (clicked, set_clicked) = create_signal(false);
    let temp_and_humidity_action = create_action(move |_: &()| {
        set_clicked(true);
        read_temp_and_humidity()
    });
    let pending = temp_and_humidity_action.pending();
    let value = temp_and_humidity_action.value();
    let result = move || value().unwrap_or(Err(Error::Unknown.into()));
    let result_view = move || {
        result().map(|answer| {
            view! {
                <p>"Temperature: " {answer.temperature} "°C"</p>
                <p>"Humidity: " {answer.humidity} "%RH"</p>
            }
        })
    };

    let on_click = move |_| {
        temp_and_humidity_action.dispatch(());
    };

    view! {
        <div id="tool-gen-random" class="tool">
            <h2>Temperature and humidity</h2>
            <button type="button" on:click=on_click>
                Read temperature and humidity
            </button>
            <Show when=move || { !pending() } fallback=|| view! { <Loading/> }>
                <Show when=clicked>
                    <ErrorBoundary fallback=errors_fallback>{result_view}</ErrorBoundary>
                </Show>
            </Show>
        </div>
    }
}
