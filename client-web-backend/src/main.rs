use std::future::IntoFuture;
use std::io::stdout;
use std::process::ExitCode;
use std::sync::Arc;
use std::{io, panic};

use anyhow::Result;
use axum::extract::{Path, Query, State};
use axum::http::{header, StatusCode};
use axum::response::{sse, IntoResponse, Redirect, Sse};
use axum::routing::{get, post};
use axum::{Json, Router};
use clap::Parser;
use client_common::{open_connection, MessageToTheBoardAndResponder};
use client_web_common::{
    GenRandomAnswer, MessageFromJsonAnswer, PingAnswer, ReadAdcAnswer,
    ReadTemepratureAndHumidityAnswer, SetRgbLedAnswer, ToggleLedAnswer,
};
use common::{MessageFromTheBoardValue, MessageToTheBoardValue};
use futures::StreamExt;
use include_dir::{include_dir, Dir};
use serde::Deserialize;
use tokio::net::TcpListener;
use tokio::sync::{broadcast, mpsc, oneshot};
use tokio::time::{sleep, Duration};
use tokio::{select, signal};
use tokio_util::sync::CancellationToken;
use tokio_util::task::TaskTracker;
use tower_http::trace::TraceLayer;
use tracing::{debug, error, info, instrument};
use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use tracing_subscriber::fmt::writer::MakeWriterExt;

use crate::cli::Args;

mod cli;

const LOGS_RAW_CHANNEL_SIZE: usize = 64;
const LOGS_HTML_CHANNEL_SIZE: usize = 64;
const SEND_MESSAGE_TIMEOUT: Duration = Duration::from_millis(1000);

// If this fails to compile, make sure you ran `trunk build` inside the
// `client-web-frontend` folder
static FRONTEND_DIR: Dir = include_dir!("$CARGO_MANIFEST_DIR/../client-web-frontend/dist");

struct AppState {
    messages_to_the_board: mpsc::Sender<MessageToTheBoardAndResponder>,
    logs_html_receiver: broadcast::Receiver<String>,
}

type SharedState = Arc<AppState>;

struct SseLogsWriter {
    sender: mpsc::Sender<Vec<u8>>,
}

impl io::Write for SseLogsWriter {
    fn write(&mut self, buf: &[u8]) -> Result<usize, io::Error> {
        let buf_vec = Vec::from(buf);
        let _ = self.sender.try_send(buf_vec);
        Ok(buf.len())
    }

    fn flush(&mut self) -> Result<(), io::Error> {
        stdout().flush()
    }
}

#[tokio::main]
async fn main() -> ExitCode {
    // We use that to print the error using tracing and not print it twice
    match main_inner().await {
        Ok(()) => ExitCode::SUCCESS,
        Err(_) => ExitCode::FAILURE,
    }
}

#[instrument(err(Debug))]
async fn main_inner() -> Result<()> {
    panic::set_hook(Box::new(|panic_info| {
        tracing::error!(%panic_info);
        std::process::exit(1);
    }));
    let (logs_raw_sender, logs_raw_receiver) = mpsc::channel(LOGS_RAW_CHANNEL_SIZE);
    let (logs_html_sender, logs_html_receiver) = broadcast::channel(LOGS_HTML_CHANNEL_SIZE);

    let sse_logs_writer = SseLogsWriter {
        sender: logs_raw_sender,
    };
    let logs_writer = std::sync::Mutex::new(sse_logs_writer).and(std::io::stdout);
    let filter = EnvFilter::builder()
        .with_default_directive(LevelFilter::INFO.into())
        .from_env_lossy();
    tracing_subscriber::fmt()
        .with_env_filter(filter)
        .with_writer(logs_writer)
        .init();
    let args = Args::parse();

    debug!(?args);

    let shutdown_token = CancellationToken::new();
    let tracker = TaskTracker::new();

    let (mut messages_from_the_board, messages_to_the_board) = open_connection(
        args.ble,
        args.serial_port,
        args.baud_rate,
        tracker.clone(),
        shutdown_token.clone(),
    )
    .await?;

    let shutdown_token_cloned = shutdown_token.clone();
    tracker.spawn(async move {
        loop {
            select! {
                _ = messages_from_the_board.recv() => (),
                _ = shutdown_token_cloned.cancelled() => {
                    break
                }
            }
        }
    });

    let shutdown_token_cloned = shutdown_token.clone();
    tracker.spawn(async move {
        convert_logs_to_html(logs_raw_receiver, logs_html_sender, shutdown_token_cloned).await
    });

    let app_state = AppState {
        messages_to_the_board,
        logs_html_receiver,
    };

    let app_state = Arc::new(app_state);

    let api = Router::new()
        .route("/message_from_json", post(handler_message_from_json))
        .route("/logs", get(handler_logs))
        .route("/gen_random", post(handler_gen_random))
        .route("/ping", post(handler_ping))
        .route("/toggle_led", post(handler_toggle_led))
        .route("/read_adc", post(handler_read_adc))
        .route(
            "/read_temperature_and_humidity",
            post(handler_read_temperature_and_humidity),
        )
        .route("/set_rgb_led", post(handler_set_rgb_led));

    let app = Router::new()
        .route("/", get(|| async { Redirect::permanent("/index.html") }))
        .nest("/api", api)
        .route("/*path", get(handler_fronted_files))
        .layer(TraceLayer::new_for_http())
        .with_state(app_state);

    let listener = TcpListener::bind(&args.bind_address).await?;
    info!("listening on {}", listener.local_addr()?);

    tracker.close();

    if args.open {
        debug!("trying to open the browser");
        if let Err(error) = open::that(args.bind_address) {
            error!(?error, "failure to open the bowser")
        }
    }

    select! {
        result = axum::serve(listener, app).into_future() => {
            if let Err(error) = result {
                error!(?error, "axum return an error");
            }
            shutdown_token.cancel();
        }
        _ = signal::ctrl_c() => {
            shutdown_token.cancel();
        }
        _ = shutdown_token.cancelled() => {},
    }

    info!("Shuting down in progress...");

    tracker.wait().await;

    info!("Shuting down completed, bye!");

    Ok(())
}

struct LinesSplitter {
    buf: Vec<u8>,
}

impl LinesSplitter {
    fn new() -> Self {
        LinesSplitter { buf: Vec::new() }
    }

    fn push(&mut self, slice: &[u8]) {
        self.buf.extend_from_slice(slice);
    }
}

impl Iterator for LinesSplitter {
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        match self.buf.iter().position(|&b| b == b'\n') {
            None => None,
            Some(n) => {
                let line = Vec::from(&self.buf[..n]);
                self.buf.drain(..n + 1);
                Some(String::from_utf8(line).unwrap_or_default())
            }
        }
    }
}

async fn convert_logs_to_html(
    mut receiver: mpsc::Receiver<Vec<u8>>,
    sender: broadcast::Sender<String>,
    shutdown_token: CancellationToken,
) {
    let inner = async move {
        let mut lines_splitter = LinesSplitter::new();
        while let Some(buf) = receiver.recv().await {
            lines_splitter.push(&buf);
            for line in lines_splitter.by_ref() {
                match ansi_to_html::convert_escaped(&line) {
                    Err(error) => error!(?error, "error while converting log line to html"),
                    Ok(line_html) => {
                        let _ = sender.send(line_html);
                    }
                }
            }
        }
    };
    select! {
        _ = inner => {}
        _ = shutdown_token.cancelled() => {}
    }
}

macro_rules! send_value_to_the_board {
    ($state:expr, $value:expr, $($pattern:pat $(if $guard:expr)? => $response:expr),+) => {
        match send_value_to_the_board(
            &$state.messages_to_the_board,
            $value,
        )
        .await
        {
            Err(e) => e.into_response(),
            $(Ok($pattern) $(if $guard)? => ($response).into_response(),)*
            Ok(m) => (
                StatusCode::BAD_GATEWAY,
                format!("Wrong answer type received: {m:?}"),
            ).into_response(),
        }
    };
}

async fn send_value_to_the_board_with_custom_timeout(
    messages_to_the_board: &mpsc::Sender<MessageToTheBoardAndResponder>,
    value: MessageToTheBoardValue,
    timeout: Duration,
) -> Result<MessageFromTheBoardValue, (StatusCode, &'static str)> {
    let (resp_tx, resp_rx) = oneshot::channel();
    match messages_to_the_board.send((value, Some(resp_tx))).await {
        Ok(()) => {
            select! {
                response = resp_rx =>{
                    response.map_err(
                        |_| (StatusCode::INTERNAL_SERVER_ERROR, "the sender was dropped, should not happen")
                    )
                }
                _ = sleep(timeout) =>
                        Err((StatusCode::GATEWAY_TIMEOUT, "timeout while waiting for the answer"))
            }
        }
        Err(_) => Err((
            StatusCode::INTERNAL_SERVER_ERROR,
            "cannot send the message to the board",
        )),
    }
}

async fn send_value_to_the_board(
    messages_to_the_board: &mpsc::Sender<MessageToTheBoardAndResponder>,
    value: MessageToTheBoardValue,
) -> Result<MessageFromTheBoardValue, (StatusCode, &'static str)> {
    send_value_to_the_board_with_custom_timeout(messages_to_the_board, value, SEND_MESSAGE_TIMEOUT)
        .await
}

async fn handler_fronted_files(Path(path): Path<String>) -> impl IntoResponse {
    let path = path.trim_start_matches('/');
    let mime_type = mime_guess::from_path(path).first_raw();

    match (FRONTEND_DIR.get_file(path), mime_type) {
        (None, _) => (StatusCode::NOT_FOUND, "").into_response(),
        (Some(file), None) => (StatusCode::OK, file.contents()).into_response(),
        (Some(file), Some(mime)) => (
            StatusCode::OK,
            [(header::CONTENT_TYPE, mime)],
            file.contents(),
        )
            .into_response(),
    }
}

async fn handler_logs(State(state): State<SharedState>) -> impl IntoResponse {
    let stream =
        tokio_stream::wrappers::BroadcastStream::new(state.logs_html_receiver.resubscribe())
            .map(|s| s.map(|s| sse::Event::default().data(s)));

    Sse::new(stream).keep_alive(
        axum::response::sse::KeepAlive::new()
            .interval(Duration::from_secs(1))
            .text("keep-alive-text"),
    )
}

async fn handler_gen_random(State(state): State<SharedState>) -> impl IntoResponse {
    send_value_to_the_board!(
        state,
        MessageToTheBoardValue::GenRandom,
        MessageFromTheBoardValue::RandomNumber(value) => Json(GenRandomAnswer{value})
    )
}

#[derive(Debug, Deserialize)]
struct PingParams {
    ping_value: u32,
}

async fn handler_ping(
    State(state): State<SharedState>,
    Query(params): Query<PingParams>,
) -> impl IntoResponse {
    send_value_to_the_board!(
        state,
        MessageToTheBoardValue::Ping(params.ping_value),
        MessageFromTheBoardValue::Pong(pong_value) => Json(PingAnswer{pong_value})
    )
}

async fn handler_toggle_led(State(state): State<SharedState>) -> impl IntoResponse {
    send_value_to_the_board!(
        state,
        MessageToTheBoardValue::ToggleLed,
        MessageFromTheBoardValue::LedToggled => Json(ToggleLedAnswer{})
    )
}

#[derive(Debug, Deserialize)]
struct ReadAdcParams {
    adc_channel_index: u8,
}

async fn handler_read_adc(
    State(state): State<SharedState>,
    Query(params): Query<ReadAdcParams>,
) -> impl IntoResponse {
    send_value_to_the_board!(
        state,
        MessageToTheBoardValue::ReadAdc(params.adc_channel_index),
        MessageFromTheBoardValue::AdcValue(adc_value) => Json(ReadAdcAnswer{adc_value}),
        MessageFromTheBoardValue::UnknownAdcChannel => (StatusCode::NOT_FOUND, "Adc channel requested not found")
    )
}

#[derive(Debug, Deserialize)]
struct SetRgbLedParams {
    r: u8,
    g: u8,
    b: u8,
}

async fn handler_set_rgb_led(
    State(state): State<SharedState>,
    Query(params): Query<SetRgbLedParams>,
) -> impl IntoResponse {
    send_value_to_the_board!(
        state,
        MessageToTheBoardValue::SetRgbLed { r: params.r, g: params.g, b: params.b },
        MessageFromTheBoardValue::RgbLedSet => Json(SetRgbLedAnswer{})
    )
}

async fn handler_read_temperature_and_humidity(
    State(state): State<SharedState>,
) -> impl IntoResponse {
    send_value_to_the_board!(
        state,
        MessageToTheBoardValue::ReadTemperatureAndHumidity,
        MessageFromTheBoardValue::TemperatureAndHumidity { temperature, humidity } =>
            Json(ReadTemepratureAndHumidityAnswer { temperature, humidity})
    )
}

#[derive(Debug, Deserialize)]
struct MessageFromJsonPayload {
    message_value: MessageToTheBoardValue,
    timout_milis: Option<u64>,
}

/// TO use this endpoint use something like
/// ```bash
/// curl -X POST --verbose http://127.0.0.1:8080/api/message_from_json -H 'Content-Type: application/json' -d'{"message_value": {"Ping": 42}}'
/// ```
async fn handler_message_from_json(
    State(state): State<SharedState>,
    Json(payload): Json<MessageFromJsonPayload>,
) -> impl IntoResponse {
    let timeout = payload
        .timout_milis
        .map(Duration::from_millis)
        .unwrap_or(SEND_MESSAGE_TIMEOUT);
    match send_value_to_the_board_with_custom_timeout(
        &state.messages_to_the_board,
        payload.message_value,
        timeout,
    )
    .await
    {
        Err(e) => e.into_response(),
        Ok(answer_value) => Json(MessageFromJsonAnswer { answer_value }).into_response(),
    }
}
