use clap::Parser;

/// A client that talk to the embedded board
#[derive(Parser, Debug)]
#[command(author, version, about)]
pub struct Args {
    ///Address on which to bind the http server
    #[arg(long, default_value = "127.0.0.1:8080")]
    pub bind_address: String,

    /// Name of the serial port to use, if not specified the first one found
    /// will be used
    #[arg(long)]
    pub serial_port: Option<String>,

    /// Baud-rate to use
    #[arg(long, default_value = "115200")]
    pub baud_rate: u32,

    /// Name or address of the BLE peripheral to connect to, if not specified
    /// the serial port is used
    #[arg(long, conflicts_with = "serial_port")]
    pub ble: Option<String>,

    /// Open the interface with the browser
    #[arg(long)]
    pub open: bool,
}
