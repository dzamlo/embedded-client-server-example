use anyhow::{anyhow, Context, Result};
use btleplug::api::{
    BDAddr, Central, Characteristic, Manager as _, Peripheral as _, ScanFilter, ValueNotification,
    WriteType,
};
use btleplug::platform::{Adapter, Manager, Peripheral};
use common::Frame;
use futures_util::{Stream, StreamExt};
use heapless::Vec;
use tokio::sync::mpsc;
use tokio::time::Duration;
use tokio::{select, time};
use tokio_util::sync::CancellationToken;
use tokio_util::task::TaskTracker;
use tracing::{debug, error, info, warn};
use uuid::Uuid;

const BLE_SCAN_MAX_TRY: u32 = 10;
const FRAMES_INPUT_UUID: Uuid = Uuid::from_u128(0x7508b965_b7a4_4902_95c1_589a7b525049);
const FRAMES_OUTPUT_UUID: Uuid = Uuid::from_u128(0x2ffd9fc8_9e3d_4f7f_b1a1_d0843f9e7ca8);

#[derive(Clone, Debug)]
enum DeviceSearchCriteria {
    Name(String),
    Address(BDAddr),
}

async fn check_peripheral(peripheral: &Peripheral, search_criteria: &DeviceSearchCriteria) -> bool {
    match search_criteria {
        DeviceSearchCriteria::Address(address) => &peripheral.address() == address,
        DeviceSearchCriteria::Name(name) => {
            if let Ok(Some(p)) = peripheral.properties().await {
                p.local_name.as_ref() == Some(name)
            } else {
                false
            }
        }
    }
}

async fn find_device(
    central: &Adapter,
    search_criteria: &DeviceSearchCriteria,
) -> Option<Peripheral> {
    for p in central.peripherals().await.unwrap() {
        if check_peripheral(&p, search_criteria).await {
            return Some(p);
        }
    }
    None
}

pub async fn open_ble(
    ble: String,
    task_tracker: TaskTracker,
    shutdown_token: CancellationToken,
    frames_writer_receiver: mpsc::Receiver<Frame>,
    frames_reader_sender: mpsc::Sender<Frame>,
) -> Result<()> {
    let search_criteria = if let Ok(address) = BDAddr::from_str_delim(&ble) {
        DeviceSearchCriteria::Address(address)
    } else {
        DeviceSearchCriteria::Name(ble)
    };
    debug!(?search_criteria, "BLE search criteria");

    let manager = Manager::new().await.unwrap();
    let central = manager
        .adapters()
        .await
        .context("failed to get BLE adapters")?
        .into_iter()
        .next()
        .ok_or(anyhow!("cannot find a BLE adapter"))?;
    central
        .start_scan(ScanFilter::default())
        .await
        .context("failed to start BLE scan")?;

    let mut peripheral = None;
    for i in 0..BLE_SCAN_MAX_TRY {
        debug!("try {i} to find BLE device");
        peripheral = find_device(&central, &search_criteria).await;
        if peripheral.is_some() {
            break;
        }
        time::sleep(Duration::from_secs(1)).await;
    }
    central
        .stop_scan()
        .await
        .context("failed to stop BLE scan")?;

    let peripheral = peripheral.ok_or(anyhow!("BLE device not found"))?;
    info!("BLE device found!");

    peripheral.connect().await?;
    peripheral.discover_services().await?;
    let chars = peripheral.characteristics();
    let input_char = chars
        .iter()
        .find(|c| c.uuid == FRAMES_INPUT_UUID)
        .ok_or(anyhow!("input charactersitic not found"))?;
    let output_char = chars
        .iter()
        .find(|c| c.uuid == FRAMES_OUTPUT_UUID)
        .ok_or(anyhow!("output charactersitic not found"))?;

    peripheral.subscribe(output_char).await?;
    let notification_stream = peripheral.notifications().await?;

    let shutdown_token_cloned = shutdown_token.clone();
    task_tracker.spawn(async move {
        read_ble(
            notification_stream,
            frames_reader_sender,
            shutdown_token_cloned,
        )
        .await;
    });

    let input_char = input_char.clone();
    task_tracker.spawn(async move {
        write_ble(
            peripheral,
            input_char,
            frames_writer_receiver,
            shutdown_token,
        )
        .await;
    });
    Ok(())
}

async fn read_ble<T: Stream<Item = ValueNotification> + std::marker::Unpin>(
    mut notification_stream: T,
    frames_reader_sender: mpsc::Sender<Frame>,
    shutdown_token: CancellationToken,
) {
    let inner = async move {
        while let Some(notification) = notification_stream.next().await {
            debug!(?notification, "received a BLE notification");
            if notification.uuid == FRAMES_OUTPUT_UUID {
                match Vec::from_slice(&notification.value[..]) {
                    Err(()) => warn!("notification value too big"),
                    Ok(frame) => {
                        let _ = frames_reader_sender.send(frame).await;
                    }
                }
            } else {
                warn!("notification from unknown characteristic");
            }
        }
    };

    select! {
        _ = inner => (),
        _ = shutdown_token.cancelled() => (),
    }
}

async fn write_ble(
    peripheral: Peripheral,
    characteristic: Characteristic,
    mut frames_writer_receiver: mpsc::Receiver<Frame>,
    shutdown_token: CancellationToken,
) {
    let inner = async move {
        while let Some(frame) = frames_writer_receiver.recv().await {
            if let Err(e) = peripheral
                .write(&characteristic, &frame[..], WriteType::WithoutResponse)
                .await
            {
                error!(?e, "error while writing to the BLE device");
            }
        }
    };
    select! {
        _ = inner => (),
        _ = shutdown_token.cancelled() => (),
    }
}
