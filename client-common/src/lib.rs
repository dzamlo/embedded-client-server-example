use std::collections::HashMap;
use std::sync::{Arc, Mutex};

use anyhow::Result;
use common::{
    check_crc, write_crc, Frame, MessageFromTheBoard, MessageFromTheBoardValue, MessageToTheBoard,
    MessageToTheBoardValue, FRAME_CRC_LENGTH,
};
use tokio::select;
use tokio::sync::{mpsc, oneshot};
use tokio_util::sync::CancellationToken;
use tokio_util::task::TaskTracker;
use tracing::{debug, error, info, warn};

use crate::ble::open_ble;
use crate::uart::open_serial_port;

const MESSAGE_FROM_THE_BOARD_CHANNEL_SIZE: usize = 32;
const MESSAGE_TO_THE_BOARD_CHANNEL_SIZE: usize = 32;
const FRAMES_READER_CHANNEL_SIZE: usize = 32;
const FRAMES_WRITER_CHANNEL_SIZE: usize = 32;

mod ble;
mod uart;

type SharedOneShoteMap = Arc<Mutex<HashMap<u32, oneshot::Sender<MessageFromTheBoardValue>>>>;
pub type MessageToTheBoardAndResponder = (
    MessageToTheBoardValue,
    Option<oneshot::Sender<MessageFromTheBoardValue>>,
);

pub async fn open_connection(
    ble: Option<String>,
    port_name: Option<String>,
    baud_rate: u32,
    task_tracker: TaskTracker,
    shutdown_token: CancellationToken,
) -> Result<(
    mpsc::Receiver<MessageFromTheBoardValue>,
    mpsc::Sender<MessageToTheBoardAndResponder>,
)> {
    let (frames_writer_sender, frames_writer_receiver) =
        mpsc::channel::<Frame>(FRAMES_WRITER_CHANNEL_SIZE);

    let (frames_reader_sender, frames_reader_receiver) =
        mpsc::channel::<Frame>(FRAMES_READER_CHANNEL_SIZE);

    if let Some(ble) = ble {
        open_ble(
            ble,
            task_tracker.clone(),
            shutdown_token.clone(),
            frames_writer_receiver,
            frames_reader_sender,
        )
        .await?;
    } else {
        open_serial_port(
            port_name,
            baud_rate,
            task_tracker.clone(),
            shutdown_token.clone(),
            frames_writer_receiver,
            frames_reader_sender,
        )
        .await?;
    }

    let (messages_from_the_board_sender, message_from_the_board_receiver) =
        mpsc::channel::<MessageFromTheBoardValue>(MESSAGE_FROM_THE_BOARD_CHANNEL_SIZE);

    let (messages_to_the_board_sender, message_to_the_board_receiver) =
        mpsc::channel::<MessageToTheBoardAndResponder>(MESSAGE_TO_THE_BOARD_CHANNEL_SIZE);

    let shared_one_shot_map = Arc::new(Mutex::new(HashMap::new()));

    let shared_one_shot_map_cloned = shared_one_shot_map.clone();
    let shutdown_token_cloned = shutdown_token.clone();
    task_tracker.spawn(async move {
        read_frames(
            frames_reader_receiver,
            shared_one_shot_map_cloned,
            messages_from_the_board_sender,
            shutdown_token_cloned,
        )
        .await;
    });

    task_tracker.spawn(async move {
        write_frames(
            frames_writer_sender,
            shared_one_shot_map,
            message_to_the_board_receiver,
            shutdown_token,
        )
        .await;
    });

    Ok((
        message_from_the_board_receiver,
        messages_to_the_board_sender,
    ))
}

async fn read_frames(
    mut frames_reader: mpsc::Receiver<Frame>,
    shared_one_shot_map: SharedOneShoteMap,
    messages_sender: mpsc::Sender<MessageFromTheBoardValue>,
    shutdown_token: CancellationToken,
) {
    let shutdown_token_cloned = shutdown_token.clone();
    let inner = async move {
        loop {
            let received = match frames_reader.recv().await {
                None => {
                    warn!("frames_reader closed, exiting the process");
                    shutdown_token_cloned.cancel();
                    break;
                }
                Some(frame) => frame,
            };
            debug!(message = "frames received", ?received);

            if !check_crc(&received) {
                warn!("invalid crc in received frame")
            } else {
                let frame = &received[..received.len() - FRAME_CRC_LENGTH];
                match postcard::from_bytes::<MessageFromTheBoard>(frame) {
                    Err(error) => {
                        warn!(message = "Error while deserializing the message", ?error)
                    }
                    Ok(received_message) => {
                        info!(message = "Message received", ?received_message);
                        if let Some(id) = received_message.response_to {
                            let maybe_receiver = shared_one_shot_map.lock().unwrap().remove(&id);
                            match maybe_receiver {
                                None => {
                                    debug!(message = "message whitout reciever for the id", id);

                                    if messages_sender.send(received_message.value).await.is_err() {
                                        debug!("messages from the board channel closed");
                                    }
                                }
                                Some(receiver) => {
                                    let _ = receiver.send(received_message.value);
                                }
                            }
                        } else if messages_sender.send(received_message.value).await.is_err() {
                            debug!("messages from the board channel closed");
                        }
                    }
                }
            }
        }
    };

    select! {
        _ = inner => (),
        _ = shutdown_token.cancelled() => (),
    }
}

async fn write_frames(
    frames_writer: mpsc::Sender<Frame>,
    shared_one_shot_map: SharedOneShoteMap,
    mut messages_receiver: mpsc::Receiver<MessageToTheBoardAndResponder>,
    shutdown_token: CancellationToken,
) {
    let mut next_id = 1;
    let inner = async move {
        loop {
            match messages_receiver.recv().await {
                None => {
                    debug!("MessageToTheBoard channel closed");
                    break;
                }
                Some((message_value_to_send, maybe_one_shot_sender)) => {
                    let id = next_id;
                    let message_to_send = MessageToTheBoard {
                        id,
                        value: message_value_to_send,
                    };
                    next_id += 1;

                    info!(message = "sending a message", ?message_to_send);
                    match postcard::to_vec(&message_to_send) {
                        Err(error) => {
                            error!(message = "error while serializing the message", ?error)
                        }
                        Ok(mut frame) => {
                            if let Err(error) = write_crc(&mut frame) {
                                error!(message = "Error while appending the crc", ?error);
                                continue;
                            }

                            debug!(message = "sending a frame", ?frame);

                            if let Some(one_shot_sender) = maybe_one_shot_sender {
                                shared_one_shot_map
                                    .lock()
                                    .unwrap()
                                    .insert(id, one_shot_sender);
                            }
                            if frames_writer.send(frame).await.is_err() {
                                debug!("Frames writer channel closed");
                                break;
                            }
                        }
                    }
                }
            }
        }
    };
    select! {
        _ = inner => (),
        _ = shutdown_token.cancelled() => (),
    }
}
