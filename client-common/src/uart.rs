use std::pin::pin;

use anyhow::{anyhow, Context, Result};
use common::{slip_encode, SlipDecoder, MAX_FRAME_SIZE};
use tokio::io::{AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt};
use tokio::select;
use tokio::sync::mpsc;
use tokio_serial::{available_ports, SerialPortBuilderExt};
use tokio_util::sync::CancellationToken;
use tokio_util::task::TaskTracker;
use tracing::{debug, error, info, warn};

use crate::*;

fn get_serial_port(port_name: Option<String>) -> Result<String> {
    match port_name {
        Some(p) => Ok(p),
        None => match available_ports()
            .context("cannot list the available ports")?
            .first()
        {
            None => Err(anyhow!("no port found")),
            Some(port) => Ok(port.port_name.clone()),
        },
    }
}

pub async fn open_serial_port(
    port_name: Option<String>,
    baud_rate: u32,
    task_tracker: TaskTracker,
    shutdown_token: CancellationToken,
    frames_writer_receiver: mpsc::Receiver<Frame>,
    frames_reader_sender: mpsc::Sender<Frame>,
) -> Result<()> {
    let port_name = get_serial_port(port_name).context("Cannot get a serial port name")?;
    info!(port_name, "Opening the serial port");
    let serial_port = tokio_serial::new(port_name, baud_rate)
        .open_native_async()
        .context("Error while opening the serial port")?;

    let (serial_port_rx, serial_port_tx) = tokio::io::split(serial_port);

    let shutdown_token_cloned = shutdown_token.clone();
    task_tracker.spawn(async move {
        read_serial_port(serial_port_rx, frames_reader_sender, shutdown_token_cloned).await;
    });

    task_tracker.spawn(async move {
        write_serial_port(serial_port_tx, frames_writer_receiver, shutdown_token).await;
    });

    Ok(())
}

async fn read_serial_port<T: AsyncRead>(
    serial_port: T,
    frames_reader_sender: mpsc::Sender<Frame>,
    shutdown_token: CancellationToken,
) {
    let shutdown_token_cloned = shutdown_token.clone();
    let inner = async move {
        let mut serial_port = pin!(serial_port);
        let mut buffer = [0; MAX_FRAME_SIZE];
        let mut decoder = SlipDecoder::new();
        loop {
            let n = serial_port
                .read(&mut buffer[..])
                .await
                .context("Error while reading the serail port")
                .unwrap();
            if n == 0 {
                warn!("EOF reached, exiting the process");
                shutdown_token_cloned.cancel();
                break;
            }

            let received = &buffer[..n];
            debug!(message = "bytes received", ?received);

            decoder.push(received);

            while let Some(decoded) = decoder.next_frame() {
                debug!(message = "frame received", ?decoded);
                let _ = frames_reader_sender.send(decoded).await;
            }
        }
    };

    select! {
        _ = inner => (),
        _ = shutdown_token.cancelled() => (),
    }
}

async fn write_serial_port<T: AsyncWrite>(
    serial_port: T,
    mut frames_writer_receiver: mpsc::Receiver<Frame>,
    shutdown_token: CancellationToken,
) {
    let shutdown_token_cloned = shutdown_token.clone();
    let inner = async move {
        let mut serial_port = pin!(serial_port);
        loop {
            match frames_writer_receiver.recv().await {
                None => {
                    debug!("Frames writer channel closed");
                    break;
                }
                Some(frame) => match slip_encode(&frame) {
                    Ok(frame_encoded) => {
                        debug!(message = "sending a frame", ?frame_encoded);
                        if let Err(error) = serial_port.write_all(&frame_encoded).await {
                            error!(
                                message = "error while writing to the serial port, exiting",
                                ?error
                            );
                            shutdown_token_cloned.cancel();
                        }
                    }
                    Err(error) => {
                        error!(message = "error while slip encoding the frame", ?error)
                    }
                },
            }
        }
    };
    select! {
        _ = inner => (),
        _ = shutdown_token.cancelled() => (),
    }
}
