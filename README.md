# embedded client server

For new project, take a look at [postcard-rpc](https://github.com/jamesmunns/postcard-rpc).

An example of how to communicate with an embedded board via a serial port

## Running the sample:

### Client
```
cd client
RUST_BACKTRACE=full RUST_LOG=trace cargo run --release
```

### Web client

First install some stuff:
```
cargo install trunk
rustup target add wasm32-unknown-unknown
```

Then build and run the code:
```
cd client-web-frontend
trunk build
cd ../client-web-backend
RUST_LOG=debug cargo run --release -- --open
```

### Board
#### Stm32
First make sure you followed instructions in the [embedded rust book](https://docs.rust-embedded.org/book/intro/install.html).
Then install some stuff:
```
cargo install probe-rs --features cli
cargo install flip-link
rustup target add thumbv7em-none-eabihf
```

Then run the code:
```
cd board-stm32
DEFMT_LOG=trace cargo run --release
```

#### ESP32-C3-Devkit-Rust, UART

```
cargo install probe-rs --features cli
cd board-esp32-c3-devkit-rust
DEFMT_LOG=debug cargo run --release -F no-wfi
```

#### ESP32-C3-Devkit-Rust, BLE

```
cargo install --git https://github.com/esp-rs/espflash.git espflash
cd board-esp32-c3-devkit-rust-ble
DEFMT_LOG=debug ESP_LOGLEVEL=trace cargo run --release
```

And pass the `--ble ESP32-C3` argument to the client.

### Testing a client without a physical board

Run the `board-virtual` project:
```
cd board-virtual
cargo run
```

Note the name of the slave device in the command output (should start with `/dev/pts/`). Pass it to the (web) client
via the `--serial-port` option.

## License

Licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in the work by you, as defined in the Apache-2.0
license, shall be dual licensed as above, without any additional terms or
conditions.
