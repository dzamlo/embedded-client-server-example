use std::io::{Read, Write};
use std::panic;

use anyhow::{anyhow, Context, Result};
use common::{
    check_crc, slip_encode, write_crc, MessageFromTheBoard, MessageFromTheBoardValue,
    MessageToTheBoard, MessageToTheBoardValue, SlipDecoder, FRAME_CRC_LENGTH, MAX_FRAME_SIZE,
};
use nix::fcntl::OFlag;
use nix::pty::{posix_openpt, ptsname_r, unlockpt};
use tracing::{debug, error, info};
use tracing_subscriber::filter::{EnvFilter, LevelFilter};

fn main() -> Result<()> {
    panic::set_hook(Box::new(|panic_info| {
        tracing::error!(%panic_info);
        std::process::exit(1);
    }));
    let filter = EnvFilter::builder()
        .with_default_directive(LevelFilter::INFO.into())
        .from_env_lossy();
    tracing_subscriber::fmt().with_env_filter(filter).init();

    let mut master_fd = posix_openpt(OFlag::O_RDWR)?;
    unlockpt(&master_fd)?;
    let slave_name = ptsname_r(&master_fd)?;

    info!(slave_name, "virtual serial port created");

    let mut decoder = SlipDecoder::new();
    let mut buf = [0u8; MAX_FRAME_SIZE];

    loop {
        let n = master_fd.read(&mut buf[..])?;
        if n == 0 {
            break;
        }
        let received = &buf[..n];
        debug!(?received, "received byte");
        decoder.push(received);
        while let Some(mut frame) = decoder.next_frame() {
            debug!(?frame, "received frame");
            if check_crc(&frame) {
                frame.truncate(frame.len() - FRAME_CRC_LENGTH);
                match postcard::from_bytes(&frame) {
                    Err(e) => error!("error while deserializing a message: {}", e),
                    Ok(received_message) => {
                        info!(?received_message, "Message received");
                        handle_message(received_message, &mut master_fd)?;
                    }
                }
            } else {
                error!("received frame with invalid crc");
            }
        }
    }

    Ok(())
}

fn handle_message<W: Write>(message: MessageToTheBoard, output: &mut W) -> Result<()> {
    use MessageFromTheBoardValue::*;
    use MessageToTheBoardValue::*;
    let message_value = match message.value {
        Ping(value) => Pong(value),
        _ => Unsupported,
    };

    let message_to_send: MessageFromTheBoard = (message.id, message_value).into();
    info!(?message_to_send, "sending a message");

    let mut frame = postcard::to_vec(&message_to_send)
        .map_err(|e| anyhow!("{e}"))
        .context("error while serializing a message")?;
    write_crc(&mut frame)
        .map_err(|e| anyhow!("{e:?}"))
        .context("error while appending the crc")?;
    let encoded_frame = slip_encode(&frame)
        .map_err(|e| anyhow!("{e:?}"))
        .context("error while slip encodigna frame")?;
    debug!("sending bytes: {:?}", encoded_frame);
    output
        .write_all(&encoded_frame)
        .context("error while wrinting the bytes to the fake serial port")?;
    Ok(())
}
