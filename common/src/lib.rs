#![no_std]

use core::array;

use crc::{Crc, CRC_32_ISCSI};
use defmt::Format;
use heapless::{Deque, Vec};
use serde::{Deserialize, Serialize};

pub const FRAME_CRC: Crc<u32> = Crc::<u32>::new(&CRC_32_ISCSI);
pub const FRAME_CRC_LENGTH: usize = 4;
pub const MAX_FRAME_SIZE: usize = 64;
pub const SLIP_END: u8 = 0xC0;
pub const SLIP_ESC: u8 = 0xDB;
pub const SLIP_ESC_END: u8 = 0xDC;
pub const SLIP_ESC_ESC: u8 = 0xDD;
pub const I2C_BUF_SIZE: usize = 64;

pub type Frame = Vec<u8, MAX_FRAME_SIZE>;

#[derive(Clone, Serialize, Deserialize, Debug, Format)]
pub struct MessageToTheBoard {
    pub id: u32,
    pub value: MessageToTheBoardValue,
}

#[derive(Clone, Serialize, Deserialize, Debug, Format)]
pub enum MessageToTheBoardValue {
    Ping(u32),
    ToggleLed,
    ReadAdc(u8),
    SetRgbLed {
        r: u8,
        g: u8,
        b: u8,
    },
    GenRandom,
    ReadI2c {
        address: u8,
        read_length: u8,
    },
    WriteI2c {
        address: u8,
        bytes: Vec<u8, I2C_BUF_SIZE>,
    },
    WriteReadI2c {
        address: u8,
        bytes: Vec<u8, I2C_BUF_SIZE>,
        read_length: u8,
    },
    ReadTemperatureAndHumidity,
}

#[derive(Clone, Serialize, Deserialize, Debug, Format)]
pub struct MessageFromTheBoard {
    pub response_to: Option<u32>,
    pub value: MessageFromTheBoardValue,
}

impl From<MessageFromTheBoardValue> for MessageFromTheBoard {
    fn from(value: MessageFromTheBoardValue) -> Self {
        MessageFromTheBoard {
            response_to: None,
            value,
        }
    }
}
impl From<(u32, MessageFromTheBoardValue)> for MessageFromTheBoard {
    fn from((id, value): (u32, MessageFromTheBoardValue)) -> Self {
        MessageFromTheBoard {
            response_to: Some(id),
            value,
        }
    }
}

#[derive(Clone, Serialize, Deserialize, Debug, Format)]
pub enum MessageFromTheBoardValue {
    ButtonPressed,
    Pong(u32),
    LedToggled,
    AdcValue(u16),
    UnknownAdcChannel,
    RgbLedSet,
    RandomNumber(u32),
    WriteReadI2cFailure,
    WriteI2cSuccess,
    WriteReadI2cSuccess(Vec<u8, I2C_BUF_SIZE>),
    TemperatureAndHumidity { temperature: f32, humidity: f32 },
    Unsupported,
}

#[derive(Clone, Copy, Debug, Format)]
pub enum SlipEncodeError {
    FrameTooSmall,
}

pub fn slip_encode(unecoded_frame: &[u8]) -> Result<Vec<u8, MAX_FRAME_SIZE>, SlipEncodeError> {
    let mut result = Vec::new();
    result.push(SLIP_END).unwrap();
    for &b in unecoded_frame {
        if result.len() == MAX_FRAME_SIZE {
            return Err(SlipEncodeError::FrameTooSmall);
        }

        match b {
            SLIP_END | SLIP_ESC => {
                if result.len() == MAX_FRAME_SIZE - 1 {
                    return Err(SlipEncodeError::FrameTooSmall);
                }
                result.push(SLIP_ESC).unwrap();
                if b == SLIP_END {
                    result.push(SLIP_ESC_END).unwrap();
                } else {
                    result.push(SLIP_ESC_ESC).unwrap();
                }
            }
            b => result.push(b).unwrap(),
        };
    }

    if result.len() == MAX_FRAME_SIZE {
        return Err(SlipEncodeError::FrameTooSmall);
    }
    result.push(SLIP_END).unwrap();

    Ok(result)
}

#[derive(Clone, Copy, Debug, Format)]
pub enum SlipDecodeError {
    InvalidEscapedCharacter,
}

/// Decode a slip encoded frame.
/// Assume the 0xC0 byte is not present
/// Return the length of the decodded message
pub fn slip_decode(encoded_frame: &mut [u8]) -> Result<usize, SlipDecodeError> {
    let mut shift = 0;
    let mut i = 0;

    while i < encoded_frame.len() {
        if encoded_frame[i] == SLIP_ESC {
            shift += 1;
            i += 1;
            match encoded_frame[i] {
                SLIP_ESC_ESC => encoded_frame[i] = SLIP_ESC,
                SLIP_ESC_END => encoded_frame[i] = SLIP_END,
                _ => return Err(SlipDecodeError::InvalidEscapedCharacter),
            }
        }

        if shift > 0 {
            encoded_frame[i - shift] = encoded_frame[i];
        }
        i += 1
    }

    Ok(encoded_frame.len() - shift)
}

pub struct SlipDecoder {
    buffer: Deque<u8, MAX_FRAME_SIZE>,
}

impl Default for SlipDecoder {
    fn default() -> Self {
        Self::new()
    }
}

impl SlipDecoder {
    pub fn new() -> SlipDecoder {
        SlipDecoder {
            buffer: Deque::new(),
        }
    }

    /// Assume that the len of bytes is <= MAX_FRAME_SIZE and that next_frame is
    /// called until it returns None between each call.
    pub fn push(&mut self, bytes: &[u8]) {
        for &b in bytes {
            if self.buffer.push_back(b).is_err() {
                self.buffer.pop_front();
                let _ = self.buffer.push_back(b);
            }
        }
    }

    /// Return the next frame
    /// Frame are already decodded
    /// Empty and invalid frames are discarded
    /// If the frame is None, there is no more frame
    pub fn next_frame(&mut self) -> Option<Vec<u8, MAX_FRAME_SIZE>> {
        let mut result = Vec::new();
        loop {
            match self.buffer.pop_front() {
                None => {
                    // If we reach the end of of the buffer, we need to put back the bytes we
                    // poped.
                    for &b in result.iter().rev() {
                        self.buffer.push_front(b).unwrap();
                    }
                    return None;
                }
                Some(b) => {
                    let result_len = result.len();
                    if b != SLIP_END {
                        result.push(b).unwrap();
                    } else if let Ok(n) = slip_decode(&mut result[..result_len]) {
                        result.truncate(n);
                        if n > 0 {
                            return Some(result);
                        }
                    }
                }
            }
        }
    }
}

#[derive(Clone, Copy, Debug, Format)]
pub enum WriteCrcError {
    FrameTooSmall,
}
pub fn write_crc(buf: &mut Vec<u8, MAX_FRAME_SIZE>) -> Result<(), WriteCrcError> {
    let crc = FRAME_CRC.checksum(&buf[..]);
    let crc_bytes = crc.to_le_bytes();
    if buf.extend_from_slice(&crc_bytes).is_err() {
        Err(WriteCrcError::FrameTooSmall)
    } else {
        Ok(())
    }
}

pub fn check_crc(frame: &[u8]) -> bool {
    if frame.len() < FRAME_CRC_LENGTH {
        return false;
    }
    let computed_crc = FRAME_CRC.checksum(&frame[..frame.len() - FRAME_CRC_LENGTH]);
    let stored_crc = u32::from_le_bytes(array::from_fn(|i| {
        frame[frame.len() - FRAME_CRC_LENGTH + i]
    }));
    computed_crc == stored_crc
}
