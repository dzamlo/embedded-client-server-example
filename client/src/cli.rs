use clap::Parser;

/// A client that talk to the embedded board
#[derive(Parser, Debug)]
#[command(author, version, about)]
pub struct Args {
    /// Name of the serial port to use, if not specified the first one found
    /// will be used
    #[arg(short, long)]
    pub serial_port: Option<String>,

    /// Baud-rate to use
    #[arg(short, long, default_value = "115200")]
    pub baud_rate: u32,

    /// Name or address of the BLE peripheral to connect to, if not specified
    /// the serial port is used
    #[arg(long, conflicts_with = "serial_port")]
    pub ble: Option<String>,
}
