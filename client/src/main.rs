use std::panic;
use std::process::ExitCode;

use anyhow::Result;
use clap::Parser;
use client_common::{open_connection, MessageToTheBoardAndResponder};
use common::MessageToTheBoardValue;
use tokio::sync::mpsc;
use tokio::time::{sleep, Duration};
use tokio::{select, signal};
use tokio_util::sync::CancellationToken;
use tokio_util::task::TaskTracker;
use tracing::{debug, info, instrument};
use tracing_subscriber::filter::{EnvFilter, LevelFilter};

use crate::cli::Args;

mod cli;

#[tokio::main]
async fn main() -> ExitCode {
    // We use that to print the error using tracing and not print it twice
    match main_inner().await {
        Ok(()) => ExitCode::SUCCESS,
        Err(_) => ExitCode::FAILURE,
    }
}

#[instrument(err(Debug))]
async fn main_inner() -> Result<()> {
    panic::set_hook(Box::new(|panic_info| {
        tracing::error!(%panic_info);
        std::process::exit(1);
    }));
    let filter = EnvFilter::builder()
        .with_default_directive(LevelFilter::INFO.into())
        .from_env_lossy();
    tracing_subscriber::fmt().with_env_filter(filter).init();
    let args = Args::parse();
    info!("Client started");

    debug!(?args);

    let shutdown_token = CancellationToken::new();
    let tracker = TaskTracker::new();

    let (mut messages_from_the_board, messages_to_the_board) = open_connection(
        args.ble,
        args.serial_port,
        args.baud_rate,
        tracker.clone(),
        shutdown_token.clone(),
    )
    .await?;

    let shutdown_token_cloned = shutdown_token.clone();
    tracker.spawn(async move {
        loop {
            select! {
                _ = messages_from_the_board.recv() => (),
                _ = shutdown_token_cloned.cancelled() => {
                    break
                }
            }
        }
    });

    let messages_to_the_board_cloned = messages_to_the_board.clone();
    let shutdown_token_cloned = shutdown_token.clone();
    tracker.spawn(async move {
        ping(messages_to_the_board_cloned, shutdown_token_cloned).await;
    });

    let messages_to_the_board_cloned = messages_to_the_board.clone();
    let shutdown_token_cloned = shutdown_token.clone();
    tracker.spawn(async move {
        toogle_led(messages_to_the_board_cloned, shutdown_token_cloned).await;
    });

    tracker.close();

    select! {
        _ = signal::ctrl_c() => {
            shutdown_token.cancel();
        }
        _ = shutdown_token.cancelled() => {},
    }

    info!("Shuting down in progress...");

    tracker.wait().await;

    info!("Shuting down completed, bye!");

    Ok(())
}

async fn ping(
    messages_sender: mpsc::Sender<MessageToTheBoardAndResponder>,
    shutdown_token: CancellationToken,
) {
    for i in 0..u32::MAX {
        messages_sender
            .send((MessageToTheBoardValue::Ping(i), None))
            .await
            .unwrap();
        select! {
            _ = sleep(Duration::from_millis(2000)) => (),
            _ = shutdown_token.cancelled() => {
                break
            }
        }
    }
}

async fn toogle_led(
    messages_sender: mpsc::Sender<MessageToTheBoardAndResponder>,
    shutdown_token: CancellationToken,
) {
    loop {
        messages_sender
            .send((MessageToTheBoardValue::ToggleLed, None))
            .await
            .unwrap();
        select! {
            _ = sleep(Duration::from_millis(1200)) => (),
            _ = shutdown_token.cancelled() => {
                break
            }
        }
    }
}
