#![no_std]
#![no_main]
#![feature(async_closure)]
#![feature(type_alias_impl_trait)]

use dhcp_server::DhcpServer;
use domain::base::iana::{Class, Opcode as DomainOpcode, Rcode};
use domain::base::{
    Message as DomainMessage, MessageBuilder as DomainMessageBuilder, Record as DomainRecord,
    Rtype, Ttl,
};
use domain::rdata::A;
use embassy_executor::Spawner;
use embassy_net::udp::{PacketMetadata, UdpSocket};
use embassy_net::{Config, Ipv4Address, Ipv4Cidr, Stack, StackResources, StaticConfigV4};
use embassy_sync::mutex::Mutex;
use embassy_time::{Duration, Timer};
use embedded_svc::wifi::{AccessPointConfiguration, Configuration, Wifi};
use esp_println::logger::init_logger_from_env;
use esp_wifi::wifi::{WifiApDevice, WifiController, WifiDevice, WifiEvent, WifiState};
use esp_wifi::{initialize, EspWifiInitFor};
use hal::clock::ClockControl;
use hal::peripherals::Peripherals;
use hal::prelude::*;
use hal::timer::TimerGroup;
use hal::{embassy, interrupt, peripherals, Rng, IO};
use http::AppState;
use log::{info, warn};
use smoltcp::wire::{Ipv4Address as SmoltcpIpv4Address, DHCP_CLIENT_PORT, DHCP_SERVER_PORT};
use static_cell::make_static;
use {esp_backtrace as _, esp_println as _};

mod dhcp_server;
mod http;

const BOARD_IP: Ipv4Address = Ipv4Address::new(192, 168, 42, 1);
const DNS_MAX_SIZE: usize = 512;
const DNS_PORT: u16 = 53;
const DNS_TTL: Ttl = Ttl::MINUTE;
const NUMBER_OF_SOCKETS: usize = http::WEB_TASK_POOL_SIZE + 2;

#[main]
async fn main(spawner: Spawner) {
    init_logger_from_env();
    let peripherals = Peripherals::take();
    let system = peripherals.SYSTEM.split();
    let clocks = ClockControl::max(system.clock_control).freeze();

    let timer_group0 = TimerGroup::new(peripherals.TIMG0, &clocks);
    embassy::init(&clocks, timer_group0.timer0);

    let ip_config = Config::ipv4_static(StaticConfigV4 {
        address: Ipv4Cidr::new(BOARD_IP, 24),
        gateway: None,
        dns_servers: Default::default(),
    });

    let timer = hal::systimer::SystemTimer::new(peripherals.SYSTIMER).alarm0;
    let init = initialize(
        EspWifiInitFor::Wifi,
        timer,
        Rng::new(peripherals.RNG),
        system.radio_clock_control,
        &clocks,
    )
    .unwrap();

    let wifi = peripherals.WIFI;
    let (wifi_interface, controller) =
        esp_wifi::wifi::new_with_mode(&init, wifi, WifiApDevice).unwrap();

    let seed = 424242;

    let stack = &*make_static!(Stack::new(
        wifi_interface,
        ip_config,
        make_static!(StackResources::<NUMBER_OF_SOCKETS>::new()),
        seed
    ));

    spawner.must_spawn(connection(controller));
    spawner.must_spawn(net_task(&stack));
    spawner.must_spawn(dhcp_task(&stack));
    spawner.must_spawn(dns_task(&stack));

    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);
    let led = io.pins.gpio7.into_push_pull_output().degrade();
    let _button = io.pins.gpio9.into_pull_down_input();
    interrupt::enable(peripherals::Interrupt::GPIO, interrupt::Priority::Priority1).unwrap();

    let state = AppState {
        led: make_static!(Mutex::new(led)),
    };

    http::serve(spawner, &stack, state).await;
}

#[embassy_executor::task]
async fn connection(mut controller: WifiController<'static>) {
    loop {
        match esp_wifi::wifi::get_wifi_state() {
            WifiState::ApStarted => {
                // wait until we're no longer connected
                controller.wait_for_event(WifiEvent::ApStop).await;
                Timer::after(Duration::from_millis(5000)).await
            }
            _ => {}
        }
        if !matches!(controller.is_started(), Ok(true)) {
            let client_config = Configuration::AccessPoint(AccessPointConfiguration {
                ssid: "esp-wifi".try_into().unwrap(),
                ..Default::default()
            });
            controller.set_configuration(&client_config).unwrap();
            controller.start().await.unwrap();
            info!("Wifi started!");
        }
    }
}

#[embassy_executor::task]
async fn net_task(stack: &'static Stack<WifiDevice<'static, WifiApDevice>>) {
    stack.run().await
}

#[embassy_executor::task]
async fn dhcp_task(stack: &'static Stack<WifiDevice<'static, WifiApDevice>>) {
    let mut dhcp_server = DhcpServer::new(
        SmoltcpIpv4Address(BOARD_IP.0),
        SmoltcpIpv4Address([255, 255, 255, 0]),
        SmoltcpIpv4Address([192, 168, 42, 2]),
        SmoltcpIpv4Address([192, 168, 42, 254]),
        60,
    );

    let mut rx_buffer = [0; 4096];
    let mut tx_buffer = [0; 4096];
    let mut rx_meta = [PacketMetadata::EMPTY; 16];
    let mut tx_meta = [PacketMetadata::EMPTY; 16];

    let mut buf = [0; 4096];
    let mut socket = UdpSocket::new(
        stack,
        &mut rx_meta,
        &mut rx_buffer,
        &mut tx_meta,
        &mut tx_buffer,
    );
    socket
        .bind((Ipv4Address::BROADCAST, DHCP_SERVER_PORT))
        .unwrap();

    loop {
        let (n, endpoint) = socket.recv_from(&mut buf).await.unwrap();
        info!("DHCP: Received {} bytes from {}", n, endpoint);
        if let Some(response) = dhcp_server.process(&buf[..n]) {
            let _ = socket
                .send_to(&response, (Ipv4Address::BROADCAST, DHCP_CLIENT_PORT))
                .await;
        }
    }
}

#[embassy_executor::task]
async fn dns_task(stack: &'static Stack<WifiDevice<'static, WifiApDevice>>) {
    let a_value = A::from_octets(BOARD_IP.0[0], BOARD_IP.0[1], BOARD_IP.0[2], BOARD_IP.0[3]);

    let mut rx_buffer = [0; 4096];
    let mut tx_buffer = [0; 4096];
    let mut rx_meta = [PacketMetadata::EMPTY; 16];
    let mut tx_meta = [PacketMetadata::EMPTY; 16];

    let mut recv_buf = [0; DNS_MAX_SIZE];
    let mut socket = UdpSocket::new(
        stack,
        &mut rx_meta,
        &mut rx_buffer,
        &mut tx_meta,
        &mut tx_buffer,
    );
    socket.bind(DNS_PORT).unwrap();

    loop {
        let (n, endpoint) = socket.recv_from(&mut recv_buf).await.unwrap();
        info!("DNS: Received {} bytes from {}", n, endpoint);
        let query_bytes = &recv_buf[..n];
        match DomainMessage::from_slice(query_bytes) {
            Err(e) => warn!("DNS: Failure to parse message: {}", e),
            Ok(m) => {
                if m.header().opcode() != DomainOpcode::Query {
                    warn!("DNS: wrong query opcode: {}", m.header().opcode());
                    continue;
                }
                let response_bytes: octseq::Array<DNS_MAX_SIZE> = octseq::Array::new();
                let response_builder = DomainMessageBuilder::from_target(response_bytes).unwrap();
                match response_builder.start_answer(&m, Rcode::NoError) {
                    Err(e) => warn!("DNS: failure to start the answer: {}", e),
                    Ok(mut response_builder) => {
                        let mut answer_count = 0;
                        for question in m.question() {
                            if let Ok(question) = question {
                                if question.qtype() == Rtype::A {
                                    info!("DNS: answering with board ip for {}", question.qname());
                                    let record = DomainRecord::new(
                                        question.qname(),
                                        Class::In,
                                        DNS_TTL,
                                        a_value.clone(),
                                    );
                                    let _ = response_builder.push(record);
                                    answer_count += 1;
                                } else {
                                    info!(
                                        "DNS: question with type != A: {}, {}",
                                        question.qtype(),
                                        question.qname()
                                    );
                                }
                            }
                        }
                        info!("DNS: Sending response with {} answers", answer_count);
                        let bytes = response_builder.finish();
                        let _ = socket.send_to(bytes.as_slice(), endpoint).await;
                    }
                }
            }
        };
    }
}
