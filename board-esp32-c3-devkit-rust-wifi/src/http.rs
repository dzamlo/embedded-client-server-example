use embassy_executor::Spawner;
use embassy_net::Stack;
use embassy_sync::blocking_mutex::raw::CriticalSectionRawMutex;
use embassy_sync::mutex::Mutex;
use embassy_time::Duration;
use esp_wifi::wifi::{WifiApDevice, WifiDevice};
use hal::gpio::{AnyPin, Output, PushPull};
use hal::prelude::*;
use picoserve::extract::{Query, State};
use picoserve::response::{File, IntoResponse, Json};
use picoserve::routing::{get, post};
use serde::{Deserialize, Serialize};
use static_cell::make_static;

// A lot of the code comes from 
// https://github.com/sammhicks/picoserve/blob/main/examples/hello_world_embassy/src/main.rs

pub const WEB_TASK_POOL_SIZE: usize = 8;

#[derive(Clone)]
pub struct AppState {
    pub led: &'static Mutex<CriticalSectionRawMutex, AnyPin<Output<PushPull>>>,
}

struct EmbassyTimer;

impl picoserve::Timer for EmbassyTimer {
    type Duration = embassy_time::Duration;
    type TimeoutError = embassy_time::TimeoutError;

    async fn run_with_timeout<F: core::future::Future>(
        &mut self,
        duration: Self::Duration,
        future: F,
    ) -> Result<F::Output, Self::TimeoutError> {
        embassy_time::with_timeout(duration, future).await
    }
}

#[derive(Serialize)]
struct EmptyResponse {}

async fn handler_toggle_led(State(state): State<AppState>) -> impl IntoResponse {
    state.led.lock().await.toggle().unwrap();
    Json(EmptyResponse {})
}

#[derive(Deserialize)]
struct PingParams {
    ping_value: u32,
}

#[derive(Serialize)]
struct PingResponse {
    pong_value: u32,
}

async fn handler_ping(Query(params): Query<PingParams>) -> impl IntoResponse {
    Json(PingResponse{pong_value: params.ping_value})
}

type AppRouter = impl picoserve::routing::PathRouter<AppState>;

fn make_app() -> picoserve::Router<AppRouter, AppState> {
    picoserve::Router::new()
        .route(
            "/",
            get(|| async { File::html(include_str!("../../client-web-frontend/dist/index.html")) }),
        )
        .route(
            "/client-web-frontend.js",
            get(|| async {
                File::javascript(include_str!(
                    "../../client-web-frontend/dist/client-web-frontend.js"
                ))
            }),
        )
        .route(
            "/client-web-frontend_bg.wasm",
            get(|| async {
                File::with_content_type(
                    "application/wasm",
                    include_bytes!("../../client-web-frontend/dist/client-web-frontend_bg.wasm"),
                )
            }),
        )
        .route("/api/ping", post(handler_ping))
        .route("/api/toggle_led", post(handler_toggle_led))
}

pub async fn serve(
    spawner: Spawner,
    stack: &'static Stack<WifiDevice<'static, WifiApDevice>>,
    state: AppState,
) {
    let app = make_static!(make_app());
    let config = make_static!(picoserve::Config {
        start_read_request_timeout: Some(Duration::from_secs(5)),
        read_request_timeout: Some(Duration::from_secs(1)),
        write_timeout: Some(Duration::from_secs(1)),
    });

    for id in 0..WEB_TASK_POOL_SIZE {
        spawner.must_spawn(web_task(id, stack, app, config, state.clone()));
    }
}

//type AppRouter = impl picoserve::routing::PathRouter<AppState>;

#[embassy_executor::task(pool_size = WEB_TASK_POOL_SIZE)]
async fn web_task(
    id: usize,
    stack: &'static Stack<WifiDevice<'static, WifiApDevice>>,
    app: &'static picoserve::Router<AppRouter, AppState>,
    config: &'static picoserve::Config<Duration>,
    state: AppState,
) -> ! {
    let mut rx_buffer = [0; 1024];
    let mut tx_buffer = [0; 1024];

    loop {
        let mut socket = embassy_net::tcp::TcpSocket::new(stack, &mut rx_buffer, &mut tx_buffer);

        log::info!("HTTP({id}): Listening on TCP:80...");
        if let Err(e) = socket.accept(80).await {
            log::warn!("HTTP({id}): accept error: {:?}", e);
            continue;
        }

        log::info!(
            "HTTP({id}): Received connection from {:?}",
            socket.remote_endpoint()
        );

        let (socket_rx, socket_tx) = socket.split();

        match picoserve::serve_with_state(
            &app,
            EmbassyTimer,
            &config,
            &mut [0; 2048],
            socket_rx,
            socket_tx,
            &state,
        )
        .await
        {
            Ok(handled_requests_count) => {
                log::info!(
                    "HTTP({id}): {handled_requests_count} requests handled from {:?}",
                    socket.remote_endpoint()
                );
            }
            Err(err) => log::error!("{err:?}"),
        }
    }
}
