use heapless::Vec;
use log::{info, warn};
use smoltcp::wire::{DhcpMessageType, DhcpPacket, DhcpRepr, EthernetAddress, Ipv4Address};

const DHCP_MAX_LEASES: usize = 8;
const DHCP_MAX_PACKET_SIZE: usize = 1024;
const IP_NULL: Ipv4Address = Ipv4Address([0, 0, 0, 0]);

type Lease = (EthernetAddress, Ipv4Address);

fn repr_to_bytes(repr: DhcpRepr) -> Vec<u8, DHCP_MAX_PACKET_SIZE> {
    let len = repr.buffer_len();
    let mut bytes = Vec::new();
    bytes.resize_default(len).unwrap();
    let mut packet = DhcpPacket::new_unchecked(&mut bytes);
    repr.emit(&mut packet).unwrap();
    bytes
}

pub struct DhcpServer {
    server_ip: Ipv4Address,
    subnet_mask: Ipv4Address,
    first_ip: Ipv4Address,
    last_ip: Ipv4Address,
    lease_duration_secs: u32,
    leases: Vec<Lease, DHCP_MAX_LEASES>,
}

impl DhcpServer {
    pub fn new(
        server_ip: Ipv4Address,
        subnet_mask: Ipv4Address,
        first_ip: Ipv4Address,
        last_ip: Ipv4Address,
        lease_duration_secs: u32,
    ) -> DhcpServer {
        DhcpServer {
            server_ip,
            subnet_mask,
            first_ip,
            last_ip,
            lease_duration_secs,
            leases: Vec::new(),
        }
    }

    fn find_exisitng_lease(&self, hardware_address: EthernetAddress) -> Option<Ipv4Address> {
        for lease in &self.leases {
            if lease.0 == hardware_address {
                return Some(lease.1);
            }
        }
        None
    }

    fn is_ip_available(&self, ip: Ipv4Address) -> bool {
        for lease in &self.leases {
            if lease.1 == ip {
                return false;
            }
        }
        true
    }

    fn get_ip(&mut self, hardware_address: EthernetAddress) -> Option<Ipv4Address> {
        if let Some(ip) = self.find_exisitng_lease(hardware_address) {
            info!("DHCP: Existing lease for {}: {}", hardware_address, ip);
            return Some(ip);
        }

        if self.leases.len() < DHCP_MAX_LEASES {
            let start = u32::from_be_bytes(self.first_ip.0);
            let end = u32::from_be_bytes(self.last_ip.0);

            'ip: for n in start..=end {
                let ip = Ipv4Address(n.to_be_bytes());
                if !self.is_ip_available(ip) {
                    continue 'ip;
                }

                self.leases.push((hardware_address, ip)).unwrap();
                info!("DHCP: New lease for {}: {}", hardware_address, ip);
                return Some(ip);
            }
            warn!("DHCP: No available ip found for {}.", hardware_address);
        } else {
            warn!(
                "DHCP: Max number of leases reached. No lease for {}.",
                hardware_address
            );
        }

        None
    }

    fn make_offer(
        &self,
        transaction_id: u32,
        client_hardware_address: EthernetAddress,
        client_identifier: Option<EthernetAddress>,
        client_ip: Ipv4Address,
    ) -> DhcpRepr {
        DhcpRepr {
            message_type: DhcpMessageType::Offer,
            transaction_id,
            client_hardware_address,
            client_ip: IP_NULL,
            your_ip: client_ip,
            server_ip: IP_NULL,
            router: Some(self.server_ip),
            subnet_mask: Some(self.subnet_mask),
            relay_agent_ip: IP_NULL,
            secs: 0,
            broadcast: false,
            requested_ip: None,
            client_identifier,
            server_identifier: Some(self.server_ip),
            parameter_request_list: None,
            dns_servers: Some(Vec::from_slice(&[self.server_ip]).unwrap()),
            max_size: None,
            renew_duration: None,
            rebind_duration: None,
            lease_duration: Some(self.lease_duration_secs),
            additional_options: &[],
        }
    }

    fn make_ack(
        &self,
        transaction_id: u32,
        client_hardware_address: EthernetAddress,
        client_identifier: Option<EthernetAddress>,
        client_ip: Ipv4Address,
    ) -> DhcpRepr {
        DhcpRepr {
            message_type: DhcpMessageType::Ack,
            transaction_id,
            client_hardware_address,
            client_ip: IP_NULL,
            your_ip: client_ip,
            server_ip: IP_NULL,
            router: Some(self.server_ip),
            subnet_mask: Some(self.subnet_mask),
            relay_agent_ip: IP_NULL,
            secs: 0,
            broadcast: false,
            requested_ip: None,
            client_identifier,
            server_identifier: Some(self.server_ip),
            parameter_request_list: None,
            dns_servers: Some(Vec::from_slice(&[self.server_ip]).unwrap()),
            max_size: None,
            renew_duration: None,
            rebind_duration: None,
            lease_duration: Some(self.lease_duration_secs),
            additional_options: &[],
        }
    }

    fn make_nack(&self, transaction_id: u32, client_hardware_address: EthernetAddress) -> DhcpRepr {
        DhcpRepr {
            message_type: DhcpMessageType::Nak,
            transaction_id,
            client_hardware_address,
            client_ip: IP_NULL,
            your_ip: IP_NULL,
            server_ip: IP_NULL,
            router: None,
            subnet_mask: None,
            relay_agent_ip: IP_NULL,
            secs: 0,
            broadcast: false,
            requested_ip: None,
            client_identifier: None,
            server_identifier: Some(self.server_ip),
            parameter_request_list: None,
            dns_servers: None,
            max_size: None,
            renew_duration: None,
            rebind_duration: None,
            lease_duration: None,
            additional_options: &[],
        }
    }

    pub fn process(&mut self, packet: &[u8]) -> Option<Vec<u8, DHCP_MAX_PACKET_SIZE>> {
        if let Ok(p) = DhcpPacket::new_checked(packet) {
            if let Ok(repr) = DhcpRepr::parse(&p) {
                let hardware_address = repr.client_hardware_address;
                let transaction_id = repr.transaction_id;
                match repr.message_type {
                    DhcpMessageType::Discover => {
                        if let Some(ip) = self.get_ip(hardware_address) {
                            let offer = self.make_offer(
                                transaction_id,
                                hardware_address,
                                repr.client_identifier,
                                ip,
                            );
                            return Some(repr_to_bytes(offer));
                        }
                    }
                    DhcpMessageType::Request => {
                        let response_repr = match self.find_exisitng_lease(hardware_address) {
                            None => {
                                info!("DHCP: No lease found, returning Nak");
                                self.make_nack(transaction_id, hardware_address)
                            }
                            Some(ip) => {
                                if Some(ip) == repr.requested_ip {
                                    info!(
                                        "DHCP: Lease found for {}, returning Ack: {}",
                                        hardware_address, ip
                                    );
                                    self.make_ack(
                                        transaction_id,
                                        hardware_address,
                                        repr.client_identifier,
                                        ip,
                                    )
                                } else {
                                    info!("DHCP: Lease found buf IP doesn't match, returning Nak");
                                    self.make_nack(transaction_id, hardware_address)
                                }
                            }
                        };
                        return Some(repr_to_bytes(response_repr));
                    }
                    _ => (),
                }
            } else {
                warn!("DHCP: Invalid DHCP packet");
            }
        } else {
            warn!("DHCP: Invalid DHCP packet size");
        }
        None
    }
}
