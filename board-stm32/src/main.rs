#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]

use common::{
    check_crc, slip_encode, write_crc, MessageFromTheBoard, MessageFromTheBoardValue,
    MessageToTheBoard, MessageToTheBoardValue, SlipDecoder, FRAME_CRC_LENGTH, MAX_FRAME_SIZE,
};
use defmt::*;
use embassy_executor::Spawner;
use embassy_stm32::adc::{Adc, Resolution};
use embassy_stm32::exti::ExtiInput;
use embassy_stm32::gpio::{Input, Level, Output, Pull, Speed};
use embassy_stm32::usart::{Config, Uart, UartRx, UartTx};
use embassy_stm32::{bind_interrupts, peripherals, usart};
use embassy_sync::blocking_mutex::raw::ThreadModeRawMutex;
use embassy_sync::channel::Channel;
use embassy_time::{Delay, Duration, Timer};
use {defmt_rtt as _, panic_probe as _};

const READ_ERROR_RETY_TIMEOUT: Duration = Duration::from_millis(100);
const MESSAGE_TO_SEND_CHANNEL_SIZE: usize = 8;
const MESSAGE_RECEIVED_CHANNEL_SIZE: usize = 8;

bind_interrupts!(struct Irqs {
    USART2 => usart::InterruptHandler<peripherals::USART2>;
});

static MESSAGE_TO_SEND_CHANNEL: Channel<
    ThreadModeRawMutex,
    MessageFromTheBoard,
    MESSAGE_TO_SEND_CHANNEL_SIZE,
> = Channel::new();
static MESSAGE_RECEIVED_CHANNEL: Channel<
    ThreadModeRawMutex,
    MessageToTheBoard,
    MESSAGE_RECEIVED_CHANNEL_SIZE,
> = Channel::new();

#[embassy_executor::main]
async fn main(spawner: Spawner) {
    info!("Board started!");

    let p = embassy_stm32::init(Default::default());

    let button = Input::new(p.PC13, Pull::Up);
    let button = ExtiInput::new(button, p.EXTI13);
    let mut led = Output::new(p.PA5, Level::High, Speed::Low);

    let mut adc = Adc::new(p.ADC1, &mut Delay);
    adc.set_resolution(Resolution::TwelveBit);
    let mut channel0 = adc.enable_temperature();
    let mut channel1 = p.PA0;

    let config = Config::default();
    let uart = Uart::new(p.USART2, p.PA3, p.PA2, Irqs, p.DMA1_CH7, p.DMA1_CH6, config).unwrap();
    let (uart_tx, uart_rx) = uart.split();

    spawner.spawn(task_uart_tx(uart_tx)).unwrap();
    spawner.spawn(task_uart_rx(uart_rx)).unwrap();

    spawner.spawn(task_button_pressed(button)).unwrap();

    loop {
        let MessageToTheBoard { id, value } = MESSAGE_RECEIVED_CHANNEL.receive().await;
        use MessageFromTheBoardValue::*;
        use MessageToTheBoardValue::*;
        match value {
            Ping(value) => MESSAGE_TO_SEND_CHANNEL.send((id, Pong(value)).into()).await,
            ToggleLed => {
                led.toggle();
                MESSAGE_TO_SEND_CHANNEL.send((id, LedToggled).into()).await;
            }
            ReadAdc(channel_index) => {
                let maybe_adc_value = match channel_index {
                    0 => Some(adc.read(&mut channel0)),
                    1 => Some(adc.read(&mut channel1)),
                    _ => None,
                };
                let message_value = match maybe_adc_value {
                    Some(adc_value) => AdcValue(adc_value),
                    None => UnknownAdcChannel,
                };
                MESSAGE_TO_SEND_CHANNEL
                    .send((id, message_value).into())
                    .await;
            }
            _ => MESSAGE_TO_SEND_CHANNEL.send((id, Unsupported).into()).await,
        }
    }
}

#[embassy_executor::task]
async fn task_uart_tx(mut uart: UartTx<'static, peripherals::USART2, peripherals::DMA1_CH7>) {
    loop {
        let message = MESSAGE_TO_SEND_CHANNEL.receive().await;
        info!("sending message: {}", message);
        match postcard::to_vec(&message) {
            Err(e) => error!("error while serializing a message {}", e),
            Ok(mut frame) => {
                if let Err(e) = write_crc(&mut frame) {
                    error!("error while appending the crc: {}", e);
                    continue;
                }

                match slip_encode(&frame) {
                    Err(e) => error!("error while slip encoding a frame {}", e),
                    Ok(encoded_frame) => {
                        debug!("sending bytes: {:x}", encoded_frame);
                        if let Err(e) = uart.write(&encoded_frame).await {
                            error!("error while writing to uart: {}", e);
                        }
                    }
                }
            }
        }
    }
}

#[embassy_executor::task]
async fn task_uart_rx(mut uart: UartRx<'static, peripherals::USART2, peripherals::DMA1_CH6>) {
    let mut decoder = SlipDecoder::new();
    let mut buf = [0u8; MAX_FRAME_SIZE];
    loop {
        match uart.read_until_idle(&mut buf).await {
            Err(e) => {
                error!("error while reading the uart: {}", e);
                Timer::after(READ_ERROR_RETY_TIMEOUT).await;
            }

            Ok(n) => {
                if n == 0 {
                    error!("empty read, waiting before retrying");
                    Timer::after(READ_ERROR_RETY_TIMEOUT).await;
                } else {
                    let received = &buf[..n];
                    debug!("received byte: {:x}", received);
                    decoder.push(received);
                    while let Some(mut frame) = decoder.next_frame() {
                        debug!("received frame: {:x}", frame);
                        if check_crc(&frame) {
                            frame.truncate(frame.len() - FRAME_CRC_LENGTH);
                            match postcard::from_bytes(&frame) {
                                Err(e) => error!("error while deserializing a message: {}", e),
                                Ok(message) => {
                                    info!("message received: {}", message);
                                    MESSAGE_RECEIVED_CHANNEL.send(message).await;
                                }
                            }
                        } else {
                            error!("received frame with invalid crc");
                        }
                    }
                }
            }
        }
    }
}

#[embassy_executor::task]
async fn task_button_pressed(mut button: ExtiInput<'static, peripherals::PC13>) {
    loop {
        button.wait_for_falling_edge().await;
        MESSAGE_TO_SEND_CHANNEL
            .send(MessageFromTheBoardValue::ButtonPressed.into())
            .await;
    }
}
