#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]

use core::panic::PanicInfo;

use common::{
    check_crc, slip_encode, write_crc, MessageFromTheBoard, MessageFromTheBoardValue,
    MessageToTheBoard, MessageToTheBoardValue, SlipDecoder, FRAME_CRC_LENGTH, I2C_BUF_SIZE,
    MAX_FRAME_SIZE,
};
use defmt::{debug, error, info};
use defmt_rtt as _;
use embassy_executor::Spawner;
#[cfg(feature = "no-wfi")]
use embassy_futures::yield_now;
use embassy_sync::blocking_mutex::raw::CriticalSectionRawMutex;
use embassy_sync::channel::Channel;
use embassy_time::{Duration, Timer};
use embedded_hal_async::digital::Wait;
use embedded_io_async::{Read as AsyncRead, Write as AsyncWrite};
use esp_hal_smartled::{smartLedBuffer, SmartLedsAdapter};
use futures_util::future::{select, Either};
use futures_util::pin_mut;
use hal::adc::{AdcConfig, Attenuation, ADC, ADC1};
use hal::assist_debug::DebugAssist;
use hal::clock::ClockControl;
use hal::gpio::{GpioPin, Input, PullDown, IO};
use hal::i2c::I2C;
use hal::peripherals::Peripherals;
use hal::prelude::*;
use hal::rmt::Rmt;
use hal::systimer::SystemTimer;
use hal::{adc, embassy, interrupt, peripherals, Rng, UsbSerialJtag};
use heapless::Vec;
use smart_leds::{SmartLedsWrite, RGB};

const READ_ERROR_RETY_TIMEOUT: Duration = Duration::from_millis(100);
const MESSAGE_TO_SEND_CHANNEL_SIZE: usize = 8;
const MESSAGE_RECEIVED_CHANNEL_SIZE: usize = 8;

static MESSAGE_TO_SEND_CHANNEL: Channel<
    CriticalSectionRawMutex,
    MessageFromTheBoard,
    MESSAGE_TO_SEND_CHANNEL_SIZE,
> = Channel::new();
static MESSAGE_RECEIVED_CHANNEL: Channel<
    CriticalSectionRawMutex,
    MessageToTheBoard,
    MESSAGE_RECEIVED_CHANNEL_SIZE,
> = Channel::new();

static BYTES_FROM_THE_UART: Channel<CriticalSectionRawMutex, u8, MAX_FRAME_SIZE> = Channel::new();
static BYTES_TO_THE_UART: Channel<CriticalSectionRawMutex, Vec<u8, MAX_FRAME_SIZE>, 1> =
    Channel::new();

#[main]
async fn main(spawner: Spawner) {
    let peripherals = Peripherals::take();
    let system = peripherals.SYSTEM.split();
    let clocks = ClockControl::boot_defaults(system.clock_control).freeze();

    extern "C" {
        // top of stack
        static mut _stack_start: u32;
        // bottom of stack
        static mut _stack_end: u32;
    }

    let stack_top = unsafe { &mut _stack_start } as *mut _ as u32;
    let stack_bottom = unsafe { &mut _stack_end } as *mut _ as u32;

    let mut da = DebugAssist::new(peripherals.ASSIST_DEBUG);
    da.enable_sp_monitor(stack_bottom + 4096, stack_top);

    interrupt::enable(
        peripherals::Interrupt::ASSIST_DEBUG,
        interrupt::Priority::Priority3,
    )
    .unwrap();

    embassy::init(&clocks, SystemTimer::new(peripherals.SYSTIMER));

    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);
    let mut led = io.pins.gpio7.into_push_pull_output();
    let button = io.pins.gpio9.into_pull_down_input();
    interrupt::enable(peripherals::Interrupt::GPIO, interrupt::Priority::Priority1).unwrap();

    let rmt = Rmt::new(peripherals.RMT, 80u32.MHz(), &clocks).unwrap();
    let mut rmt_buffer = smartLedBuffer!(1);
    let mut rgb_led = SmartLedsAdapter::new(rmt.channel0, io.pins.gpio2, &mut rmt_buffer);

    let analog = peripherals.APB_SARADC.split();
    let mut adc1_config = AdcConfig::new();
    let atten = Attenuation::Attenuation11dB;
    type AdcCal = adc::AdcCalCurve<ADC1>;
    let mut pin1 = adc1_config.enable_pin_with_cal::<_, AdcCal>(io.pins.gpio3.into_analog(), atten);
    let mut pin2 = adc1_config.enable_pin_with_cal::<_, AdcCal>(io.pins.gpio4.into_analog(), atten);
    let mut adc1 = ADC::<ADC1>::adc(analog.adc1, adc1_config).unwrap();

    let mut rng = Rng::new(peripherals.RNG);

    let mut i2c0 = I2C::new(
        peripherals.I2C0,
        io.pins.gpio10,
        io.pins.gpio8,
        400u32.kHz(),
        &clocks,
    );

    interrupt::enable(
        peripherals::Interrupt::I2C_EXT0,
        interrupt::Priority::Priority1,
    )
    .unwrap();

    let mut i2c_buf: Vec<u8, I2C_BUF_SIZE> = Vec::new();
    let _ = i2c_buf.resize_default(I2C_BUF_SIZE);

    interrupt::enable(
        peripherals::Interrupt::USB_DEVICE,
        interrupt::Priority::Priority1,
    )
    .unwrap();

    let usb_serial = UsbSerialJtag::new(peripherals.USB_DEVICE);
    #[cfg(feature = "no-wfi")]
    spawner.spawn(keep_cpu_busy()).unwrap();
    spawner.spawn(task_uart_tx()).unwrap();
    spawner.spawn(task_uart_rx()).unwrap();
    spawner.spawn(task_actual_uart_tx_rx(usb_serial)).unwrap();

    spawner.spawn(task_button_pressed(button)).unwrap();

    loop {
        let MessageToTheBoard { id, value } = MESSAGE_RECEIVED_CHANNEL.receive().await;
        use MessageFromTheBoardValue::*;
        use MessageToTheBoardValue::*;
        match value {
            Ping(value) => MESSAGE_TO_SEND_CHANNEL.send((id, Pong(value)).into()).await,
            ToggleLed => {
                led.toggle().unwrap();
                MESSAGE_TO_SEND_CHANNEL.send((id, LedToggled).into()).await;
            }
            SetRgbLed { r, g, b } => {
                let _ = rgb_led.write([RGB::new(r, g, b)].into_iter());
                MESSAGE_TO_SEND_CHANNEL.send((id, RgbLedSet).into()).await;
            }
            ReadAdc(channel_index) => {
                // TODO: use async instead of nb::block
                let maybe_adc_value = match channel_index {
                    0 => Some(nb::block!(adc1.read(&mut pin1)).unwrap()),
                    1 => Some(nb::block!(adc1.read(&mut pin2)).unwrap()),
                    _ => None,
                };
                let message_value = match maybe_adc_value {
                    Some(adc_value) => AdcValue(adc_value),
                    None => UnknownAdcChannel,
                };
                MESSAGE_TO_SEND_CHANNEL
                    .send((id, message_value).into())
                    .await;
            }
            GenRandom => {
                let value = rng.random();
                MESSAGE_TO_SEND_CHANNEL
                    .send((id, RandomNumber(value)).into())
                    .await;
            }
            ReadI2c {
                address,
                read_length,
            } => {
                let read_length = read_length as usize;
                let value = match i2c0.read(address, &mut i2c_buf[0..read_length]) {
                    Err(e) => {
                        error!("i2c read failure: {}", e);
                        WriteReadI2cFailure
                    }
                    Ok(()) => {
                        WriteReadI2cSuccess(Vec::from_slice(&i2c_buf[0..read_length]).unwrap())
                    }
                };
                MESSAGE_TO_SEND_CHANNEL.send((id, value).into()).await;
            }
            WriteI2c { address, bytes } => {
                let value = match i2c0.write(address, bytes.as_slice()) {
                    Err(e) => {
                        error!("i2c write failure: {}", e);
                        WriteReadI2cFailure
                    }
                    Ok(()) => WriteI2cSuccess,
                };
                MESSAGE_TO_SEND_CHANNEL.send((id, value).into()).await;
            }
            WriteReadI2c {
                address,
                bytes,
                read_length,
            } => {
                let read_length = read_length as usize;
                let value = match i2c0.write_read(
                    address,
                    bytes.as_slice(),
                    &mut i2c_buf[0..read_length],
                ) {
                    Err(e) => {
                        error!("i2c write-read failure: {}", e);
                        WriteReadI2cFailure
                    }
                    Ok(()) => {
                        WriteReadI2cSuccess(Vec::from_slice(&i2c_buf[0..read_length]).unwrap())
                    }
                };
                MESSAGE_TO_SEND_CHANNEL.send((id, value).into()).await;
            }
            ReadTemperatureAndHumidity => {
                let value = read_temp_and_rh(&mut i2c0).await;
                MESSAGE_TO_SEND_CHANNEL.send((id, value).into()).await;
            }

            // We keep this pattern because with it, adding a new command doesn't break the code
            #[allow(unreachable_patterns)]
            _ => MESSAGE_TO_SEND_CHANNEL.send((id, Unsupported).into()).await,
        }
    }
}

async fn read_temp_and_rh<T: _esp_hal_i2c_Instance>(
    i2c: &mut I2C<'_, T>,
) -> MessageFromTheBoardValue {
    const SHTC3_ADDRESS: u8 = 0x70;
    const DIVISOR: f32 = (1 << 16) as f32;
    use MessageFromTheBoardValue::*;

    if i2c.write(SHTC3_ADDRESS, &[0x35, 0x17]).is_err() {
        return WriteReadI2cFailure;
    }

    Timer::after_millis(1).await;

    if i2c.write(SHTC3_ADDRESS, &[0x78, 0x66]).is_err() {
        return WriteReadI2cFailure;
    }

    Timer::after_millis(13).await;

    let mut read_buf = [0u8; 6];

    if i2c.read(SHTC3_ADDRESS, &mut read_buf[..]).is_err() {
        return WriteReadI2cFailure;
    }

    let raw_temp = ((read_buf[0] as u16) << 8) | read_buf[1] as u16;
    let temperature = -45.0 + 175.0 * raw_temp as f32 / DIVISOR;

    let raw_humidity = ((read_buf[3] as u16) << 8) | read_buf[4] as u16;
    let humidity = 100.0 * raw_humidity as f32 / DIVISOR;

    TemperatureAndHumidity {
        temperature,
        humidity,
    }
}

#[cfg(feature = "no-wfi")]
#[embassy_executor::task]
async fn keep_cpu_busy() {
    loop {
        yield_now().await;
    }
}
/// Thisn complicated sstuff is necessary because for the moment we cannont split the UsbSerialJtag
#[embassy_executor::task]
async fn task_actual_uart_tx_rx(mut usb_serial: UsbSerialJtag<'static>) {
    loop {
        let bytes = {
            let fut1 = serial_read(&mut usb_serial);
            let fut2 = BYTES_TO_THE_UART.receive();

            pin_mut!(fut1);
            pin_mut!(fut2);

            match select(fut1, fut2).await {
                Either::Left(_) => Vec::new(),
                Either::Right((bytes, _)) => bytes,
            }
        };

        if !bytes.is_empty() {
            AsyncWrite::write_all(&mut usb_serial, &bytes[..])
                .await
                .unwrap();
        }
    }
}

async fn serial_read(usb_serial: &mut UsbSerialJtag<'_>) {
    loop {
        let mut read_buf = [0; MAX_FRAME_SIZE];

        match AsyncRead::read(usb_serial, &mut read_buf).await {
            Ok(n) => {
                for &b in &read_buf[..n] {
                    let _ = BYTES_FROM_THE_UART.try_send(b);
                }
            }

            Err(e) => {
                error!("error while reading the uart: {}", e);
                Timer::after(READ_ERROR_RETY_TIMEOUT).await;
            }
        }
    }
}

#[embassy_executor::task]
async fn task_uart_tx() {
    loop {
        let message = MESSAGE_TO_SEND_CHANNEL.receive().await;
        info!("sending message: {}", message);
        match postcard::to_vec(&message) {
            Err(e) => error!("error while serializing a message {}", e),
            Ok(mut frame) => {
                if let Err(e) = write_crc(&mut frame) {
                    error!("error while appending the crc: {}", e);
                    continue;
                }

                match slip_encode(&frame) {
                    Err(e) => error!("error while slip encoding a frame {}", e),
                    Ok(encoded_frame) => {
                        debug!("sending bytes: {:x}", encoded_frame);
                        let _ = BYTES_TO_THE_UART.try_send(encoded_frame);
                    }
                }
            }
        }
    }
}

#[embassy_executor::task]
async fn task_uart_rx() {
    let mut decoder = SlipDecoder::new();
    loop {
        let b = BYTES_FROM_THE_UART.receive().await;
        decoder.push(&[b]);
        while let Some(mut frame) = decoder.next_frame() {
            debug!("received frame: {:x}", frame);
            if check_crc(&frame) {
                frame.truncate(frame.len() - FRAME_CRC_LENGTH);
                match postcard::from_bytes(&frame) {
                    Err(e) => error!("error while deserializing a message: {}", e),
                    Ok(message) => {
                        info!("message received: {}", message);
                        MESSAGE_RECEIVED_CHANNEL.send(message).await;
                    }
                }
            } else {
                error!("received frame with invalid crc");
            }
        }
    }
}

#[embassy_executor::task]
async fn task_button_pressed(mut button: GpioPin<Input<PullDown>, 9>) {
    loop {
        let _ = button.wait_for_falling_edge().await;
        MESSAGE_TO_SEND_CHANNEL
            .send(MessageFromTheBoardValue::ButtonPressed.into())
            .await;
    }
}

#[allow(unreachable_code)]
#[interrupt]
fn ASSIST_DEBUG() {
    panic!("Stack under/overflow detected");
}

#[panic_handler]
fn panic(panic_info: &PanicInfo) -> ! {
    if let Some(location) = panic_info.location() {
        error!(
            "Panic occurred in file '{}' at line {}",
            location.file(),
            location.line(),
        );
    } else {
        error!("Panic occurred at unknown location");
    }
    loop {}
}
