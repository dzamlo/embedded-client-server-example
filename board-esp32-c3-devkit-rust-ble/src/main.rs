#![no_std]
#![no_main]
#![feature(async_closure)]
#![feature(type_alias_impl_trait)]

use bleps::ad_structure::{
    create_advertising_data, AdStructure, BR_EDR_NOT_SUPPORTED, LE_GENERAL_DISCOVERABLE,
};
use bleps::async_attribute_server::AttributeServer;
use bleps::asynch::Ble;
use bleps::attribute_server::NotificationData;
use bleps::gatt;
use common::{
    check_crc, write_crc, Frame, MessageFromTheBoard, MessageFromTheBoardValue, MessageToTheBoard,
    MessageToTheBoardValue, FRAME_CRC_LENGTH, I2C_BUF_SIZE, MAX_FRAME_SIZE,
};
use defmt::{dbg, debug, error, info};
use embassy_executor::Spawner;
use embassy_sync::blocking_mutex::raw::CriticalSectionRawMutex;
use embassy_sync::channel::Channel;
use embassy_time::Timer;
use embedded_hal_async::digital::Wait;
use esp_hal_smartled::{smartLedBuffer, SmartLedsAdapter};
use esp_println::logger::init_logger_from_env;
use esp_wifi::ble::controller::asynch::BleConnector;
use esp_wifi::{initialize, EspWifiInitFor};
use hal::adc::{AdcCalCurve, AdcConfig, AdcPin, Attenuation, ADC, ADC1};
use hal::clock::ClockControl;
use hal::gpio::{Analog, GpioPin, Input, Output, PullDown, PushPull, IO};
use hal::i2c::I2C;
use hal::peripherals::{Peripherals, I2C0};
use hal::prelude::*;
use hal::rmt::Rmt;
use hal::timer::TimerGroup;
use hal::{adc, embassy, interrupt, peripherals, rmt, Rng};
use heapless::Vec;
use smart_leds::{SmartLedsWrite, RGB};
use {esp_backtrace as _, esp_println as _};

const MESSAGE_TO_SEND_CHANNEL_SIZE: usize = 8;
const MESSAGE_RECEIVED_CHANNEL_SIZE: usize = 8;
const FRAMES_FROM_BLE_CHANNEL_SIZE: usize = 8;
const FRAMES_TO_BLE_CHANNEL_SIZE: usize = 8;

static MESSAGE_TO_SEND_CHANNEL: Channel<
    CriticalSectionRawMutex,
    MessageFromTheBoard,
    MESSAGE_TO_SEND_CHANNEL_SIZE,
> = Channel::new();
static MESSAGE_RECEIVED_CHANNEL: Channel<
    CriticalSectionRawMutex,
    MessageToTheBoard,
    MESSAGE_RECEIVED_CHANNEL_SIZE,
> = Channel::new();

static FRAMES_FROM_BLE_CHANNEL: Channel<
    CriticalSectionRawMutex,
    Frame,
    FRAMES_FROM_BLE_CHANNEL_SIZE,
> = Channel::new();

static FRAMES_TO_BLE_CHANNEL: Channel<CriticalSectionRawMutex, Frame, FRAMES_TO_BLE_CHANNEL_SIZE> =
    Channel::new();

#[main]
async fn main(spawner: Spawner) {
    init_logger_from_env();
    let peripherals = Peripherals::take();
    let system = peripherals.SYSTEM.split();
    let clocks = ClockControl::max(system.clock_control).freeze();

    let timer_group0 = TimerGroup::new(peripherals.TIMG0, &clocks);
    embassy::init(&clocks, timer_group0.timer0);

    let io = IO::new(peripherals.GPIO, peripherals.IO_MUX);
    let led = io.pins.gpio7.into_push_pull_output();
    let button = io.pins.gpio9.into_pull_down_input();
    interrupt::enable(peripherals::Interrupt::GPIO, interrupt::Priority::Priority1).unwrap();

    let rmt = Rmt::new(peripherals.RMT, 80u32.MHz(), &clocks).unwrap();
    let rmt_buffer = smartLedBuffer!(1);
    let rgb_led = SmartLedsAdapter::new(rmt.channel0, io.pins.gpio2, rmt_buffer);

    let analog = peripherals.APB_SARADC.split();
    let mut adc1_config = AdcConfig::new();
    let atten = Attenuation::Attenuation11dB;
    type AdcCal = adc::AdcCalCurve<ADC1>;
    let pin1 = adc1_config.enable_pin_with_cal::<_, AdcCal>(io.pins.gpio3.into_analog(), atten);
    let pin2 = adc1_config.enable_pin_with_cal::<_, AdcCal>(io.pins.gpio4.into_analog(), atten);
    let adc1 = ADC::<ADC1>::adc(analog.adc1, adc1_config).unwrap();

    let i2c0 = I2C::new(
        peripherals.I2C0,
        io.pins.gpio10,
        io.pins.gpio8,
        400u32.kHz(),
        &clocks,
    );

    interrupt::enable(
        peripherals::Interrupt::I2C_EXT0,
        interrupt::Priority::Priority1,
    )
    .unwrap();

    spawner
        .spawn(task_handle_messages(led, rgb_led, adc1, pin1, pin2, i2c0))
        .unwrap();
    spawner.spawn(task_uart_tx()).unwrap();
    spawner.spawn(task_uart_rx()).unwrap();

    spawner.spawn(task_button_pressed(button)).unwrap();

    let timer = hal::systimer::SystemTimer::new(peripherals.SYSTIMER).alarm0;
    let init = initialize(
        EspWifiInitFor::Ble,
        timer,
        Rng::new(peripherals.RNG),
        system.radio_clock_control,
        &clocks,
    )
    .unwrap();

    let mut bluetooth = peripherals.BT;

    let connector = BleConnector::new(&init, &mut bluetooth);
    let mut ble = Ble::new(connector, esp_wifi::current_millis);

    loop {
        dbg!(ble.init().await.unwrap());
        dbg!(ble.cmd_set_le_advertising_parameters().await.unwrap());
        dbg!(ble
            .cmd_set_le_advertising_data(
                create_advertising_data(&[
                    AdStructure::Flags(LE_GENERAL_DISCOVERABLE | BR_EDR_NOT_SUPPORTED),
                    AdStructure::ServiceUuids16(&[]),
                    AdStructure::CompleteLocalName("ESP32-C3"),
                ])
                .unwrap()
            )
            .await
            .unwrap());
        dbg!(ble.cmd_set_le_advertise_enable(true).await.unwrap());

        info!("started advertising");

        let mut frame_input_write = |_offset: usize, data: &[u8]| {
            if data.len() <= MAX_FRAME_SIZE {
                let frame = Vec::from_slice(data).unwrap();
                let _ = FRAMES_FROM_BLE_CHANNEL.try_send(frame);
            }
        };

        let mut frame_output_read = |_offset: usize, _data: &mut [u8]| 0;

        gatt!([service {
            uuid: "e7042f4f-c1fe-463c-b29f-df9547288a90",
            characteristics: [
                characteristic {
                    name: "frames_input_characteristic",
                    uuid: "7508b965-b7a4-4902-95c1-589a7b525049",
                    write: frame_input_write,
                },
                characteristic {
                    name: "frames_output_characteristic",
                    uuid: "2ffd9fc8-9e3d-4f7f-b1a1-d0843f9e7ca8",
                    notify: true,
                    read: frame_output_read,
                },
            ],
        },]);

        let mut rng = bleps::no_rng::NoRng;
        let mut srv = AttributeServer::new(&mut ble, &mut gatt_attributes, &mut rng);

        let mut notifier = async || {
            let frame = FRAMES_TO_BLE_CHANNEL.receive().await;
            debug!("sending frame: {:x}", frame);
            NotificationData::new(frames_output_characteristic_handle, &frame)
        };

        srv.run(&mut notifier).await.unwrap();
    }
}

async fn read_temp_and_rh<T: _esp_hal_i2c_Instance>(
    i2c: &mut I2C<'_, T>,
) -> MessageFromTheBoardValue {
    const SHTC3_ADDRESS: u8 = 0x70;
    const DIVISOR: f32 = (1 << 16) as f32;
    use MessageFromTheBoardValue::*;

    if i2c.write(SHTC3_ADDRESS, &[0x35, 0x17]).is_err() {
        return WriteReadI2cFailure;
    }

    Timer::after_millis(1).await;

    if i2c.write(SHTC3_ADDRESS, &[0x78, 0x66]).is_err() {
        return WriteReadI2cFailure;
    }

    Timer::after_millis(13).await;

    let mut read_buf = [0u8; 6];

    if i2c.read(SHTC3_ADDRESS, &mut read_buf[..]).is_err() {
        return WriteReadI2cFailure;
    }

    let raw_temp = ((read_buf[0] as u16) << 8) | read_buf[1] as u16;
    let temperature = -45.0 + 175.0 * raw_temp as f32 / DIVISOR;

    let raw_humidity = ((read_buf[3] as u16) << 8) | read_buf[4] as u16;
    let humidity = 100.0 * raw_humidity as f32 / DIVISOR;

    TemperatureAndHumidity {
        temperature,
        humidity,
    }
}

#[embassy_executor::task]
async fn task_handle_messages(
    mut led: GpioPin<Output<PushPull>, 7>,
    mut rgb_led: SmartLedsAdapter<rmt::Channel<0>, 0, 25>,
    mut adc1: ADC<'static, ADC1>,
    mut pin1: AdcPin<GpioPin<Analog, 3>, ADC1, AdcCalCurve<ADC1>>,
    mut pin2: AdcPin<GpioPin<Analog, 4>, ADC1, AdcCalCurve<ADC1>>,
    mut i2c0: I2C<'static, I2C0>,
) {
    let mut i2c_buf: Vec<u8, I2C_BUF_SIZE> = Vec::new();
    let _ = i2c_buf.resize_default(I2C_BUF_SIZE);

    loop {
        let MessageToTheBoard { id, value } = MESSAGE_RECEIVED_CHANNEL.receive().await;
        use MessageFromTheBoardValue::*;
        use MessageToTheBoardValue::*;
        match value {
            Ping(value) => MESSAGE_TO_SEND_CHANNEL.send((id, Pong(value)).into()).await,

            ToggleLed => {
                led.toggle().unwrap();
                MESSAGE_TO_SEND_CHANNEL.send((id, LedToggled).into()).await;
            }

            SetRgbLed { r, g, b } => {
                let _ = rgb_led.write([RGB::new(r, g, b)].into_iter());
                MESSAGE_TO_SEND_CHANNEL.send((id, RgbLedSet).into()).await;
            }
            ReadAdc(channel_index) => {
                // TODO: use async instead of nb::block
                let maybe_adc_value = match channel_index {
                    0 => Some(nb::block!(adc1.read(&mut pin1)).unwrap()),
                    1 => Some(nb::block!(adc1.read(&mut pin2)).unwrap()),
                    _ => None,
                };
                let message_value = match maybe_adc_value {
                    Some(adc_value) => AdcValue(adc_value),
                    None => UnknownAdcChannel,
                };
                MESSAGE_TO_SEND_CHANNEL
                    .send((id, message_value).into())
                    .await;
            }

            ReadI2c {
                address,
                read_length,
            } => {
                let read_length = read_length as usize;
                let value = match i2c0.read(address, &mut i2c_buf[0..read_length]) {
                    Err(e) => {
                        error!("i2c read failure: {}", e);
                        WriteReadI2cFailure
                    }
                    Ok(()) => {
                        WriteReadI2cSuccess(Vec::from_slice(&i2c_buf[0..read_length]).unwrap())
                    }
                };
                MESSAGE_TO_SEND_CHANNEL.send((id, value).into()).await;
            }
            WriteI2c { address, bytes } => {
                let value = match i2c0.write(address, bytes.as_slice()) {
                    Err(e) => {
                        error!("i2c write failure: {}", e);
                        WriteReadI2cFailure
                    }
                    Ok(()) => WriteI2cSuccess,
                };
                MESSAGE_TO_SEND_CHANNEL.send((id, value).into()).await;
            }
            WriteReadI2c {
                address,
                bytes,
                read_length,
            } => {
                let read_length = read_length as usize;
                let value = match i2c0.write_read(
                    address,
                    bytes.as_slice(),
                    &mut i2c_buf[0..read_length],
                ) {
                    Err(e) => {
                        error!("i2c write-read failure: {}", e);
                        WriteReadI2cFailure
                    }
                    Ok(()) => {
                        WriteReadI2cSuccess(Vec::from_slice(&i2c_buf[0..read_length]).unwrap())
                    }
                };
                MESSAGE_TO_SEND_CHANNEL.send((id, value).into()).await;
            }
            ReadTemperatureAndHumidity => {
                let value = read_temp_and_rh(&mut i2c0).await;
                MESSAGE_TO_SEND_CHANNEL.send((id, value).into()).await;
            }

            // We keep this pattern because with it, adding a new command doesn't break the code
            #[allow(unreachable_patterns)]
            _ => MESSAGE_TO_SEND_CHANNEL.send((id, Unsupported).into()).await,
        }
    }
}

#[embassy_executor::task]
async fn task_uart_tx() {
    loop {
        let message = MESSAGE_TO_SEND_CHANNEL.receive().await;
        info!("sending message: {}", message);
        match postcard::to_vec(&message) {
            Err(e) => error!("error while serializing a message {}", e),
            Ok(mut frame) => {
                if let Err(e) = write_crc(&mut frame) {
                    error!("error while appending the crc: {}", e);
                    continue;
                }
                FRAMES_TO_BLE_CHANNEL.send(frame).await;
            }
        }
    }
}

#[embassy_executor::task]
async fn task_uart_rx() {
    loop {
        let mut frame = FRAMES_FROM_BLE_CHANNEL.receive().await;
        debug!("received frame: {:x}", frame);
        if check_crc(&frame) {
            frame.truncate(frame.len() - FRAME_CRC_LENGTH);
            match postcard::from_bytes(&frame) {
                Err(e) => error!("error while deserializing a message: {}", e),
                Ok(message) => {
                    info!("message received: {}", message);
                    MESSAGE_RECEIVED_CHANNEL.send(message).await;
                }
            }
        } else {
            error!("received frame with invalid crc");
        }
    }
}

#[embassy_executor::task]
async fn task_button_pressed(mut button: GpioPin<Input<PullDown>, 9>) {
    loop {
        let _ = button.wait_for_falling_edge().await;
        MESSAGE_TO_SEND_CHANNEL
            .send(MessageFromTheBoardValue::ButtonPressed.into())
            .await;
    }
}
